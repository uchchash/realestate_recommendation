-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2018 at 05:36 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recommendation`
--

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `d_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`d_id`, `name`) VALUES
(1, 'Dhaka'),
(2, 'Chittagong');

-- --------------------------------------------------------

--
-- Table structure for table `historical data`
--

CREATE TABLE `historical data` (
  `id` int(50) NOT NULL,
  `p_id` int(50) NOT NULL,
  `Price_1990_(Taka/sq.ft)` int(50) NOT NULL,
  `Price_1995_(Taka/sq.ft)` int(50) NOT NULL,
  `Price_2000_(Taka/sq.ft)` int(50) NOT NULL,
  `Price_2005_(Taka/sq.ft)` int(50) NOT NULL,
  `Price_2010_(Taka/sq.ft)` int(50) NOT NULL,
  `Price_2015_(Taka/sq-ft)` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historical data`
--

INSERT INTO `historical data` (`id`, `p_id`, `Price_1990_(Taka/sq.ft)`, `Price_1995_(Taka/sq.ft)`, `Price_2000_(Taka/sq.ft)`, `Price_2005_(Taka/sq.ft)`, `Price_2010_(Taka/sq.ft)`, `Price_2015_(Taka/sq-ft)`) VALUES
(1, 2, 2150, 2200, 2400, 3300, 14000, 10500),
(2, 5, 2115, 2080, 2450, 4500, 14000, 20000),
(3, 1, 1250, 1300, 1500, 2500, 5500, 7000),
(4, 3, 1650, 1750, 2000, 2700, 5300, 9000),
(5, 6, 1600, 1850, 2250, 2500, 7000, 13000);

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `p_id` int(100) NOT NULL,
  `place` varchar(100) NOT NULL,
  `d_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`p_id`, `place`, `d_id`) VALUES
(1, 'Mirpur', 1),
(2, 'Dhanmondi', 1),
(3, 'Uttara', 1),
(4, 'Bashundhara', 1),
(5, 'Gulshan', 1),
(6, 'Malibag', 1);

-- --------------------------------------------------------

--
-- Table structure for table `poi`
--

CREATE TABLE `poi` (
  `id` int(200) NOT NULL,
  `name` varchar(100) NOT NULL,
  `lattitude` double NOT NULL,
  `longitude` double NOT NULL,
  `photo` varchar(100) NOT NULL,
  `types` varchar(100) NOT NULL,
  `rating` float NOT NULL,
  `rs_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poi`
--

INSERT INTO `poi` (`id`, `name`, `lattitude`, `longitude`, `photo`, `types`, `rating`, `rs_id`) VALUES
(1, 'Centre For The Rehabilitation Of The Paralysed - Mirpur', 23.8037968, 90.3871225, '<a href=\"https://maps.google.com/maps/contrib/115697856952033413479/photos\">A.b. Parvez</a>', 'hospital', 4.3, 1),
(2, 'Institute of Science Trade & Technology', 23.8077126, 90.3827075, '<a href=\"https://maps.google.com/maps/contrib/112186461677031759856/photos\">Dipongkar Paul</a>', 'university', 4, 1),
(3, 'City Within by Navana', 23.8049815, 90.3833989, '<a href=\"https://maps.google.com/maps/contrib/118350549836891635836/photos\">Md. Hamidur Rahman (Sr. ', 'point_of_interest', 4, 1),
(4, 'Bijoy Rakeen City', 23.8043925, 90.3819808, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 1),
(5, 'Public Order Management', 23.8022285, 90.3824868, '<a href=\"https://maps.google.com/maps/contrib/101621553167652419886/photos\">Robel Akram</a>', 'police', 4, 1),
(6, 'Projonmo Hostel', 23.8079223, 90.3825223, '', 'lodging', 0, 1),
(7, 'Vashantek Govt. College', 23.8076834, 90.3851173, '<a href=\"https://maps.google.com/maps/contrib/114141555719730834669/photos\">Tania Islam</a>', 'university', 4, 1),
(8, 'Joynagar Project, National Housing Authority', 23.8081318, 90.3830328, '<a href=\"https://maps.google.com/maps/contrib/103905036213820156689/photos\">Feroze Ibne Ali</a>', 'point_of_interest', 3.2, 1),
(9, 'Central University of Science & Technology', 23.8039925, 90.3871446, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'university', 3.5, 1),
(10, 'Taskeater', 23.8037566, 90.3871272, '<a href=\"https://maps.google.com/maps/contrib/113608765078733120872/photos\">Aiman Ghazi</a>', 'point_of_interest', 3.5, 1),
(11, 'PSC Convention Hall', 23.8042594, 90.3796452, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 4.2, 1),
(12, 'University of ISTT', 23.8037771, 90.3872385, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'university', 4.3, 1),
(13, 'Police Staff College', 23.802712, 90.3802616, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'police', 4.2, 1),
(14, 'BRP - Bhashantek Rehabilitation Project', 23.8057416, 90.3875471, '<a href=\"https://maps.google.com/maps/contrib/112514969635833713773/photos\">Nazmul Huda</a>', 'lodging', 3.9, 1),
(15, 'Mirpur Cyclists Point', 23.8074742, 90.3803289, '', 'point_of_interest', 5, 1),
(16, 'National Center for Special Education , PHT Center, Jatio Protibondhi Unnoyon Faundation', 23.8030066, 90.3870947, '<a href=\"https://maps.google.com/maps/contrib/102092756546051434002/photos\">Uzzal Mozumder</a>', 'school', 3, 1),
(17, 'Sphinx Corporation', 23.807977, 90.380599, '<a href=\"https://maps.google.com/maps/contrib/109468588753974216819/photos\">Ruhul Quddus</a>', 'point_of_interest', 3.7, 1),
(18, 'Kafrul Police Station', 23.8012549, 90.3814935, '<a href=\"https://maps.google.com/maps/contrib/105505602978119898120/photos\">Abdullah Al Mamun</a>', 'police', 3.7, 1),
(19, 'True Condominum', 23.8053547, 90.3832617, '<a href=\"https://maps.google.com/maps/contrib/100043013517271259287/photos\">Hamid Rahman</a>', 'point_of_interest', 0, 1),
(20, 'RUPOSHI PRO-ACTIVE VILLAGE', 23.805648, 90.384768, '<a href=\"https://maps.google.com/maps/contrib/107669497148670126021/photos\">Waliur Rashid Tamal</a>', 'lodging', 4.6, 1),
(21, 'Pallabi Shopping Centre', 23.8245208, 90.3639328, '<a href=\"https://maps.google.com/maps/contrib/114961232641196714520/photos\">Hafiz Shuputra</a>', 'shopping_mall', 4, 2),
(22, 'Prime Bank Limited ATM', 23.8224486, 90.3641828, '<a href=\"https://maps.google.com/maps/contrib/102404769214802874337/photos\">Young Traveller&#39;s</a', 'atm', 3.7, 2),
(23, 'Pallabi M I Model High School', 23.8257865, 90.3616841, '<a href=\"https://maps.google.com/maps/contrib/104673188384016288051/photos\">Black Hwak</a>', 'school', 4.1, 2),
(24, 'Doulah & Doulah, Law Firm in Dhaka, Bangladesh', 23.824819, 90.364347, '<a href=\"https://maps.google.com/maps/contrib/117931891198332915907/photos\">Doulah &amp; Doulah, Law', 'point_of_interest', 5, 2),
(25, 'California Fried Chicken & Pastry Shop', 23.8235565, 90.3641555, '<a href=\"https://maps.google.com/maps/contrib/102728950613316115424/photos\">Zihad Tarafdar</a>', 'restaurant', 3.8, 2),
(26, 'Cantina, fayon, me&mom, Dabber', 23.8245646, 90.3643586, '<a href=\"https://maps.google.com/maps/contrib/101415674434752194821/photos\">Murshida Ferdous Binte H', 'supermarket', 3, 2),
(27, 'T.K Sports', 23.823772, 90.364485, '<a href=\"https://maps.google.com/maps/contrib/112277507383105396424/photos\">T.K SPORTS</a>', 'store', 3.8, 2),
(28, 'FASHIONEX - Fashion Exclusive Ltd.', 23.824113, 90.36106, '<a href=\"https://maps.google.com/maps/contrib/114985953345657206126/photos\">FASHIONEX - Fashion Excl', 'point_of_interest', 5, 2),
(29, 'Renata Limited (à¦°à§‡à¦¨à¦¾à¦Ÿà¦¾ à¦²à¦¿à¦®à¦¿à¦Ÿà§‡à¦¡)', 23.821857, 90.3602703, '<a href=\"https://maps.google.com/maps/contrib/111565435717135097030/photos\">durbar yaqub</a>', 'health', 4.6, 2),
(30, 'Office Automation Technology', 23.8224872, 90.3624904, '', 'point_of_interest', 0, 2),
(31, 'BRAC Bank Limited ATM', 23.824276, 90.361646, '<a href=\"https://maps.google.com/maps/contrib/107107731088201748422/photos\">à¦¶à¦“à¦•à¦¤ à¦®à§à¦°à¦', 'atm', 4.3, 2),
(32, 'X Land', 23.8242693, 90.362168, '<a href=\"https://maps.google.com/maps/contrib/106308026123995557241/photos\">Mahfuz Onty</a>', 'clothing_store', 1, 2),
(33, 'AttireBD', 23.8228205, 90.3624051, '<a href=\"https://maps.google.com/maps/contrib/110395016086196639005/photos\">AttireBD</a>', 'clothing_store', 0, 2),
(34, 'Pallabi Police Fari', 23.82399, 90.3640717, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'police', 3.2, 2),
(35, 'Softbyte IT Solution', 23.824366, 90.361579, '', 'point_of_interest', 5, 2),
(36, 'Infinitech', 23.8242232, 90.3618055, 'Photos are copyrighted by their owners', 'electronics_store', 0, 2),
(37, 'DiaBazar.com', 23.8244986, 90.3619292, '<a href=\"https://maps.google.com/maps/contrib/105612438216352555956/photos\">Shah Md. Sultan</a>', 'shopping_mall', 3.9, 2),
(38, 'WEB TECH SOFT', 23.8245014, 90.3619708, '<a href=\"https://maps.google.com/maps/contrib/115319616129642145710/photos\">green bizz</a>', 'point_of_interest', 5, 2),
(39, 'Moringa Private Limited', 23.8244582, 90.3614102, '<a href=\"https://maps.google.com/maps/contrib/100817643043092274569/photos\">S. M. Arifur Rahman</a>', 'point_of_interest', 0, 2),
(40, 'icddr,b Field Office', 23.817248, 90.3739053, '<a href=\"https://maps.google.com/maps/contrib/114430649149804926891/photos\">Jewel Rana</a>', 'point_of_interest', 4.3, 3),
(41, 'Karbala, Polash Nogor', 23.8173375, 90.3772141, '<a href=\"https://maps.google.com/maps/contrib/114350135820186889562/photos\">Md Arifur Rahman Khan</a', 'point_of_interest', 4, 3),
(42, 'Progoti High School', 23.8178908, 90.374364, '<a href=\"https://maps.google.com/maps/contrib/112945489260707785149/photos\">Make Money</a>', 'school', 3.9, 3),
(43, 'SoftTechBD Ltd.', 23.819378, 90.374027, '<a href=\"https://maps.google.com/maps/contrib/109066046503336583077/photos\">Md Maruf Adnan Sami</a>', 'point_of_interest', 5, 3),
(44, 'Sinthia Computer', 23.8189165, 90.3754663, '', 'point_of_interest', 0, 3),
(45, 'Mirpur Agrani School', 23.8174507, 90.3721668, '<a href=\"https://maps.google.com/maps/contrib/116401601502952441110/photos\">mr obaidullah</a>', 'school', 5, 3),
(46, 'Rasim Networks', 23.8161836, 90.3744334, '<a href=\"https://maps.google.com/maps/contrib/107697758357091791998/photos\">Raihan Hossain</a>', 'point_of_interest', 5, 3),
(47, 'NCSD Field Office, icddr,b', 23.8208366, 90.374125, '', 'point_of_interest', 0, 3),
(48, 'Pusti Office, icddr,b', 23.8184694, 90.3770775, '', 'point_of_interest', 0, 3),
(49, 'M/s Wasim Sanitary', 23.8187913, 90.3770756, '<a href=\"https://maps.google.com/maps/contrib/109902302829743306844/photos\">Mehedi Hassan Niaz</a>', 'point_of_interest', 4, 3),
(50, 'Best Holidays Ltd', 23.8184675, 90.3771598, '<a href=\"https://maps.google.com/maps/contrib/115497203126457050800/photos\">Haji Anwar Hossain Rony<', 'travel_agency', 5, 3),
(51, 'Smart IT Bangladesh', 23.8184871, 90.3772715, '', 'point_of_interest', 5, 3),
(52, 'Anowara Telecom', 23.8185951, 90.3772715, '<a href=\"https://maps.google.com/maps/contrib/118233999536103384422/photos\">osman goni</a>', 'real_estate_agency', 4.6, 3),
(53, 'BRAC Bank Limited ATM', 23.8213009, 90.3731284, '<a href=\"https://maps.google.com/maps/contrib/103449932324254481203/photos\">Panna Rahman</a>', 'atm', 3.7, 3),
(54, 'Standard Group (à¦¸à§à¦Ÿà§à¦¯à¦¾à¦¨à§à¦¡à¦¾à¦°à§à¦¡ à¦—à§à¦°à§à¦ª)', 23.8216843, 90.3751659, '<a href=\"https://maps.google.com/maps/contrib/101770683121234613834/photos\">Masud Rana</a>', 'point_of_interest', 4.4, 3),
(55, 'Onirban Bangladesh', 23.8214647, 90.3734225, '<a href=\"https://maps.google.com/maps/contrib/105587236503438322394/photos\">Abdullah Al Fahad</a>', 'point_of_interest', 4.8, 3),
(56, 'Reflection', 23.820471, 90.3768786, '', 'point_of_interest', 5, 3),
(57, 'Marie Stopes', 23.8214831, 90.3721243, '<a href=\"https://maps.google.com/maps/contrib/102713173221121478074/photos\">Md Almamun</a>', 'health', 4, 3),
(58, 'Kayes & Associates', 23.820867, 90.376616, '<a href=\"https://maps.google.com/maps/contrib/115107776935620094099/photos\">Kayes &amp; Associates</', 'point_of_interest', 5, 3),
(59, 'Shah Villa', 23.8153017, 90.3758086, '', 'point_of_interest', 0, 3),
(60, 'North South University', 23.815103, 90.425538, '<a href=\"https://maps.google.com/maps/contrib/117540630993561226644/photos\">North South University</', 'university', 4.5, 4),
(61, 'GP House', 23.8123408, 90.4259218, '<a href=\"https://maps.google.com/maps/contrib/100812830115690946175/photos\">Sadman_ Sakib016</a>', 'point_of_interest', 4.6, 4),
(62, 'North South University Library', 23.8150956, 90.4269784, '<a href=\"https://maps.google.com/maps/contrib/111678859978078554264/photos\">Mubassir Ahsan</a>', 'library', 4.7, 4),
(63, 'Basic Bank Limited', 23.8128213, 90.4284346, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'bank', 3.8, 4),
(64, 'China inn hotel', 23.816706, 90.426337, '', 'lodging', 0, 4),
(65, 'International Centre for Climate Change and Development', 23.8126119, 90.4300978, '<a href=\"https://maps.google.com/maps/contrib/107415293634291278788/photos\">International Centre for', 'point_of_interest', 4.2, 4),
(66, 'Thinker', 23.811407, 90.425891, '', 'point_of_interest', 2.5, 4),
(67, 'Bashundhara Corporate Office', 23.8118863, 90.4301218, '<a href=\"https://maps.google.com/maps/contrib/116719755110696390888/photos\">Mahmudul Hasan</a>', 'point_of_interest', 4.1, 4),
(68, 'Grameenphone Limited', 23.8120617, 90.4259954, '<a href=\"https://maps.google.com/maps/contrib/104556028239764196853/photos\">Hasib Rezu</a>', 'point_of_interest', 4.3, 4),
(69, 'North South University Auditorium', 23.8156336, 90.4270064, '<a href=\"https://maps.google.com/maps/contrib/106036129065840886710/photos\">Mohammad Abu Sayed</a>', 'point_of_interest', 4.5, 4),
(70, 'Genetic Mayabee', 23.8135871, 90.4290524, '<a href=\"https://maps.google.com/maps/contrib/109564497076452734030/photos\">Genetic Mayabee</a>', 'point_of_interest', 3.7, 4),
(71, 'Social Islami Bank Limited', 23.8128241, 90.4289117, '<a href=\"https://maps.google.com/maps/contrib/118292777726534178700/photos\">Syed Shafqat Rabbi</a>', 'bank', 4.3, 4),
(72, 'Southeast Bank Limited', 23.8137797, 90.4284443, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'bank', 3.8, 4),
(73, 'Eastern Bank Limited', 23.8128311, 90.4286653, '<a href=\"https://maps.google.com/maps/contrib/105655298857197313292/photos\">Ahnaf Brinto</a>', 'bank', 4.5, 4),
(74, 'Prime Bank Limited, Bosundhara Branch.', 23.8129421, 90.4269899, '<a href=\"https://maps.google.com/maps/contrib/101050510210647412274/photos\">SOHEL talukder</a>', 'bank', 4.2, 4),
(75, 'Mutual Trust Bank Limited', 23.812919, 90.4269346, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'bank', 4.3, 4),
(76, 'BRAC Bank Limited ATM', 23.8136629, 90.4282039, '<a href=\"https://maps.google.com/maps/contrib/117989094771985852080/photos\">Momenul Islam</a>', 'atm', 4, 4),
(77, 'Eastern Bank Limited ATM', 23.8128197, 90.4288558, '<a href=\"https://maps.google.com/maps/contrib/104556028239764196853/photos\">Hasib Rezu</a>', 'atm', 4.5, 4),
(78, 'GadgetGang7', 23.8128442, 90.4246735, '<a href=\"https://maps.google.com/maps/contrib/108138387223724130038/photos\">GadgetGang7</a>', 'electronics_store', 3.5, 4),
(79, 'Movenpick', 23.8126893, 90.4282117, '<a href=\"https://maps.google.com/maps/contrib/106198848214928804762/photos\">Foysal Ahmed</a>', 'food', 4.3, 4),
(80, 'Police Staff College', 23.802712, 90.3802616, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'police', 4.2, 5),
(81, 'Kafrul Police Station', 23.8012549, 90.3814935, '<a href=\"https://maps.google.com/maps/contrib/105505602978119898120/photos\">Abdullah Al Mamun</a>', 'police', 3.7, 5),
(82, 'Public Order Management', 23.8022285, 90.3824868, '<a href=\"https://maps.google.com/maps/contrib/101621553167652419886/photos\">Robel Akram</a>', 'police', 4, 5),
(83, 'Government Unani and Ayurvedic Medical College', 23.8040938, 90.3781197, '<a href=\"https://maps.google.com/maps/contrib/111794091994874898657/photos\">aliuzzaman tushar</a>', 'hospital', 3.9, 5),
(84, 'Institute of Science Trade & Technology', 23.8077126, 90.3827075, '<a href=\"https://maps.google.com/maps/contrib/112186461677031759856/photos\">Dipongkar Paul</a>', 'university', 4, 5),
(85, 'Bijoy Rakeen City', 23.8043925, 90.3819808, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 5),
(86, 'City Within by Navana', 23.8049815, 90.3833989, '<a href=\"https://maps.google.com/maps/contrib/118350549836891635836/photos\">Md. Hamidur Rahman (Sr. ', 'point_of_interest', 4, 5),
(87, 'PSC Convention Hall', 23.8042594, 90.3796452, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 4.2, 5),
(88, 'Bangladesh Krishi Bank Training Institute', 23.8035661, 90.3784838, '<a href=\"https://maps.google.com/maps/contrib/118191667416027851236/photos\">Hemanta Biswas</a>', 'bank', 3.7, 5),
(89, 'Team Travel', 23.8031672, 90.3786645, '', 'point_of_interest', 0, 5),
(90, 'President Zone', 23.8041082, 90.3782257, '<a href=\"https://maps.google.com/maps/contrib/110837407459038494424/photos\">à¦¬à¦¿à¦¨à§‹à¦¦à¦¨ à¦¬à¦', 'point_of_interest', 0, 5),
(91, 'Mirpur Cyclists Point', 23.8074742, 90.3803289, '', 'point_of_interest', 5, 5),
(92, 'Sphinx Corporation', 23.807977, 90.380599, '<a href=\"https://maps.google.com/maps/contrib/109468588753974216819/photos\">Ruhul Quddus</a>', 'point_of_interest', 3.7, 5),
(93, 'MBM Garments Ltd', 23.8025934, 90.3784319, '', 'store', 4, 5),
(94, 'Hatekhori web ltd.', 23.802168, 90.378558, '', 'point_of_interest', 0, 5),
(95, 'Projonmo Hostel', 23.8079223, 90.3825223, '', 'lodging', 0, 5),
(96, 'So Cheap Hosting', 23.802029, 90.378663, '<a href=\"https://maps.google.com/maps/contrib/105750972030885545910/photos\">So Cheap Hosting</a>', 'point_of_interest', 5, 5),
(97, 'Rishal Group of Industries', 23.8034186, 90.3777931, '', 'point_of_interest', 4, 5),
(98, 'Kalni', 23.8021271, 90.3785405, '<a href=\"https://maps.google.com/maps/contrib/105440891601708067996/photos\">Kalni</a>', 'point_of_interest', 3.6, 5),
(99, 'sknews24.com', 23.802131, 90.378493, '<a href=\"https://maps.google.com/maps/contrib/113763818822976623921/photos\">sknews24.com</a>', 'point_of_interest', 5, 5),
(100, 'Marino Hotel', 23.8637594, 90.4057196, '<a href=\"https://maps.google.com/maps/contrib/114946181082638743484/photos\">Forhad Ahmed</a>', 'lodging', 3.9, 6),
(101, 'Richmond Hotel & Suites', 23.8592696, 90.4005698, '<a href=\"https://maps.google.com/maps/contrib/110503991145201710871/photos\">MD. MAHABUB ALAM SAYED</', 'lodging', 3.8, 6),
(102, 'Comfort Inn', 23.8634845, 90.405506, '<a href=\"https://maps.google.com/maps/contrib/114013494927722129248/photos\">Hotel Comfort Inn</a>', 'lodging', 3.7, 6),
(103, 'Dutch Bangla Bank Limited ATM', 23.8608119, 90.4006274, '<a href=\"https://maps.google.com/maps/contrib/118081416268371263829/photos\">Maidul Islam</a>', 'atm', 4.2, 6),
(104, 'SYL Valley Inn', 23.8623278, 90.398998, '', 'lodging', 0, 6),
(105, 'Pizza Inn', 23.8609509, 90.3992559, '<a href=\"https://maps.google.com/maps/contrib/110076743361396152463/photos\">Ismail Bablu</a>', 'restaurant', 4.1, 6),
(106, 'Islami Bank Bangladesh Limited', 23.8649261, 90.3996706, '<a href=\"https://maps.google.com/maps/contrib/107338546288245819609/photos\">Kazi Muhshin</a>', 'bank', 4.1, 6),
(107, 'MaxRenda Apparels & Accessories Ltd.', 23.8641395, 90.3995238, '', 'point_of_interest', 0, 6),
(108, 'Uttara Bank Limited, Uttara Branch', 23.8628377, 90.3995596, '<a href=\"https://maps.google.com/maps/contrib/101724367888459891887/photos\">Mirza Khalid</a>', 'bank', 4, 6),
(109, 'The Aga Khan School', 23.8608355, 90.4024108, '<a href=\"https://maps.google.com/maps/contrib/109499377436072467906/photos\">Ahsan Zaman</a>', 'school', 4.3, 6),
(110, 'Aarong Flagship Outlet', 23.8617894, 90.3995311, '<a href=\"https://maps.google.com/maps/contrib/112115021996799214152/photos\">Zahid zico</a>', 'shopping_mall', 4.3, 6),
(111, 'Coffee World Uttara', 23.8609937, 90.3992308, '<a href=\"https://maps.google.com/maps/contrib/108437545261585998040/photos\">Habib Ullah Bahar</a>', 'cafe', 4, 6),
(112, 'DHL Uttara Service Point', 23.8654952, 90.400435, '<a href=\"https://maps.google.com/maps/contrib/117982693800395153651/photos\">Quaribul Hasan.</a>', 'point_of_interest', 4, 6),
(113, 'Berger Paints Bangladesh Limited', 23.8616624, 90.3990872, '<a href=\"https://maps.google.com/maps/contrib/103993600667129623163/photos\">CERULEAN Antar</a>', 'home_goods_store', 4.2, 6),
(114, 'Ankur ICT Development Foundation', 23.8642148, 90.4009688, '', 'point_of_interest', 4.3, 6),
(115, 'The Aga Khan School', 23.8626445, 90.4025419, '<a href=\"https://maps.google.com/maps/contrib/100958826840571883461/photos\">Sayeef Rahman</a>', 'school', 4.1, 6),
(116, 'ANNEX Travel & Consulting Group (Travel Agent- Tour Operator)', 23.8632817, 90.4003008, '<a href=\"https://maps.google.com/maps/contrib/104554642820700009497/photos\">à¦…à§à¦¯à¦¾à¦¨à§‡à¦•à§', 'travel_agency', 5, 6),
(117, 'Mutual Trust Bank Limited', 23.8617511, 90.4003042, '<a href=\"https://maps.google.com/maps/contrib/105612438216352555956/photos\">Shah Md. Sultan</a>', 'bank', 3.5, 6),
(118, 'United College of Aviation Science and Management', 23.8627185, 90.3982705, '<a href=\"https://maps.google.com/maps/contrib/107712329997587398529/photos\">Imran&#39;s Creation</a>', 'university', 4.1, 6),
(119, 'Bank Asia Limited', 23.8625676, 90.4002862, '<a href=\"https://maps.google.com/maps/contrib/103449932324254481203/photos\">Panna Rahman</a>', 'bank', 3.9, 6),
(120, 'Jinghua Bangla Tours & Travels Ltd', 23.794008, 90.413934, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'travel_agency', 4, 7),
(121, 'Embassy of Spain', 23.7940218, 90.4125499, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'embassy', 4.1, 7),
(122, 'Bata', 23.7938262, 90.4145431, '<a href=\"https://maps.google.com/maps/contrib/110290886265328164855/photos\">Syed Naved Abdullah</a>', 'shoe_store', 4.3, 7),
(123, 'The Westin Dhaka', 23.7931842, 90.4146906, '<a href=\"https://maps.google.com/maps/contrib/112730737981224409177/photos\">The Westin Dhaka</a>', 'lodging', 4.5, 7),
(124, 'Quality Inn', 23.7967872, 90.4106368, '<a href=\"https://maps.google.com/maps/contrib/102109233717354650317/photos\">Biplob Biplob</a>', 'lodging', 3.7, 7),
(125, 'Premier Bank Limited', 23.79308, 90.4155004, '<a href=\"https://maps.google.com/maps/contrib/100184291643871645549/photos\">Zeenat Islam</a>', 'bank', 4.1, 7),
(126, 'Gulshan 2 Kacha Bazar', 23.7945681, 90.4154983, '<a href=\"https://maps.google.com/maps/contrib/110874094855255188159/photos\">Shahzad ali Khurram</a>', 'point_of_interest', 3.8, 7),
(127, 'Colonial Residence (Park View)', 23.7968778, 90.4156777, '<a href=\"https://maps.google.com/maps/contrib/101126202178614772564/photos\">oranm amjuk</a>', 'lodging', 4, 7),
(128, 'Australian High Commission', 23.8001053, 90.4129599, '<a href=\"https://maps.google.com/maps/contrib/107260561261604967583/photos\">Dark Circle</a>', 'embassy', 4.2, 7),
(129, 'Embassy of the Federal Republic of Germany', 23.798818, 90.4129237, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'embassy', 4.2, 7),
(130, 'Embassy of the Netherlands', 23.7965535, 90.4149598, '<a href=\"https://maps.google.com/maps/contrib/111957403240538926778/photos\">Ahmed Galeb</a>', 'embassy', 4.4, 7),
(131, 'Gulshan Circle-2, Dhaka.', 23.7948574, 90.4142119, '<a href=\"https://maps.google.com/maps/contrib/100516462078489469127/photos\">MD. MUSFIQUR RAHMAN</a>', 'neighborhood', 4.2, 7),
(132, 'IDP Education Bangladesh Pvt Ltd', 23.7951463, 90.4147704, '<a href=\"https://maps.google.com/maps/contrib/105191075201109407407/photos\">IDP Education Bangladesh', 'point_of_interest', 3.7, 7),
(133, 'Embassy of Sweden', 23.7981787, 90.4122173, '<a href=\"https://maps.google.com/maps/contrib/107718361547306758571/photos\">MD.SHAHAB UDDIN</a>', 'embassy', 4.3, 7),
(134, 'Maple International Ltd', 23.7968315, 90.4118002, '', 'point_of_interest', 0, 7),
(135, 'Khazana', 23.7955844, 90.4119306, '<a href=\"https://maps.google.com/maps/contrib/112580956857541586694/photos\">Prabhakar Mishra</a>', 'restaurant', 4.1, 7),
(136, 'Doreen Tower Heliport', 23.7944589, 90.41383, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'airport', 3.9, 7),
(137, 'Cambrian College', 23.794173, 90.41424, '<a href=\"https://maps.google.com/maps/contrib/110874094855255188159/photos\">Shahzad ali Khurram</a>', 'university', 3.5, 7),
(138, 'Australian International School', 23.7999282, 90.4139868, '<a href=\"https://maps.google.com/maps/contrib/105768614470244482024/photos\">Aref Farook</a>', 'school', 4.3, 7),
(139, 'Unimart', 23.7959476, 90.4147721, '<a href=\"https://maps.google.com/maps/contrib/104592893675742187537/photos\">aminul Islam</a>', 'supermarket', 4.4, 7),
(140, 'Police Staff College', 23.802712, 90.3802616, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'police', 4.2, 8),
(141, 'Kafrul Police Station', 23.8012549, 90.3814935, '<a href=\"https://maps.google.com/maps/contrib/105505602978119898120/photos\">Abdullah Al Mamun</a>', 'police', 3.7, 8),
(142, 'Public Order Management', 23.8022285, 90.3824868, '<a href=\"https://maps.google.com/maps/contrib/101621553167652419886/photos\">Robel Akram</a>', 'police', 4, 8),
(143, 'Government Unani and Ayurvedic Medical College', 23.8040938, 90.3781197, '<a href=\"https://maps.google.com/maps/contrib/111794091994874898657/photos\">aliuzzaman tushar</a>', 'hospital', 3.9, 8),
(144, 'Institute of Science Trade & Technology', 23.8077126, 90.3827075, '<a href=\"https://maps.google.com/maps/contrib/112186461677031759856/photos\">Dipongkar Paul</a>', 'university', 4, 8),
(145, 'Bijoy Rakeen City', 23.8043925, 90.3819808, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 8),
(146, 'City Within by Navana', 23.8049815, 90.3833989, '<a href=\"https://maps.google.com/maps/contrib/118350549836891635836/photos\">Md. Hamidur Rahman (Sr. ', 'point_of_interest', 4, 8),
(147, 'PSC Convention Hall', 23.8042594, 90.3796452, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 4.2, 8),
(148, 'Bangladesh Krishi Bank Training Institute', 23.8035661, 90.3784838, '<a href=\"https://maps.google.com/maps/contrib/118191667416027851236/photos\">Hemanta Biswas</a>', 'bank', 3.7, 8),
(149, 'Team Travel', 23.8031672, 90.3786645, '', 'point_of_interest', 0, 8),
(150, 'President Zone', 23.8041082, 90.3782257, '<a href=\"https://maps.google.com/maps/contrib/110837407459038494424/photos\">à¦¬à¦¿à¦¨à§‹à¦¦à¦¨ à¦¬à¦', 'point_of_interest', 0, 8),
(151, 'Mirpur Cyclists Point', 23.8074742, 90.3803289, '', 'point_of_interest', 5, 8),
(152, 'Sphinx Corporation', 23.807977, 90.380599, '<a href=\"https://maps.google.com/maps/contrib/109468588753974216819/photos\">Ruhul Quddus</a>', 'point_of_interest', 3.7, 8),
(153, 'MBM Garments Ltd', 23.8025934, 90.3784319, '', 'store', 4, 8),
(154, 'Hatekhori web ltd.', 23.802168, 90.378558, '', 'point_of_interest', 0, 8),
(155, 'Projonmo Hostel', 23.8079223, 90.3825223, '', 'lodging', 0, 8),
(156, 'So Cheap Hosting', 23.802029, 90.378663, '<a href=\"https://maps.google.com/maps/contrib/105750972030885545910/photos\">So Cheap Hosting</a>', 'point_of_interest', 5, 8),
(157, 'Rishal Group of Industries', 23.8034186, 90.3777931, '', 'point_of_interest', 4, 8),
(158, 'Kalni', 23.8021271, 90.3785405, '<a href=\"https://maps.google.com/maps/contrib/105440891601708067996/photos\">Kalni</a>', 'point_of_interest', 3.6, 8),
(159, 'sknews24.com', 23.802131, 90.378493, '<a href=\"https://maps.google.com/maps/contrib/113763818822976623921/photos\">sknews24.com</a>', 'point_of_interest', 5, 8),
(160, 'Viquarunnisa Noon School and College Bashundhara Branch', 23.8205979, 90.4317413, '<a href=\"https://maps.google.com/maps/contrib/110660822343565480525/photos\">Md. Muzahid Bin-Mizan</a', 'school', 4.3, 9),
(161, 'HSBC Bank Limited ATM', 23.8220106, 90.4293195, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'atm', 4.8, 9),
(162, 'SnapShot', 23.822334, 90.433389, '<a href=\"https://maps.google.com/maps/contrib/110705391895031412986/photos\">SnapShot</a>', 'point_of_interest', 5, 9),
(163, 'Eastern Bank Limited ATM', 23.821596, 90.4293828, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'atm', 4.8, 9),
(164, 'SLOAN SQUARE', 23.8201713, 90.4318145, '<a href=\"https://maps.google.com/maps/contrib/105655298857197313292/photos\">Ahnaf Brinto</a>', 'point_of_interest', 5, 9),
(165, 'HSBC ATM Bashundhara', 23.8220168, 90.4293254, '<a href=\"https://maps.google.com/maps/contrib/110290886265328164855/photos\">Syed Naved Abdullah</a>', 'atm', 4.3, 9),
(166, 'Orange Visuals', 23.8228947, 90.4328986, '<a href=\"https://maps.google.com/maps/contrib/117026954003723073692/photos\">Orange Visuals</a>', 'point_of_interest', 0, 9),
(167, 'Greenland Kaniz Evelina', 23.8193207, 90.4294045, '<a href=\"https://maps.google.com/maps/contrib/108448402556243933986/photos\">Mmjsumit</a>', 'point_of_interest', 0, 9),
(168, 'Greenland Raihan Castle', 23.81859, 90.4318483, '<a href=\"https://maps.google.com/maps/contrib/113373166723905912626/photos\">Amirul Islam</a>', 'point_of_interest', 0, 9),
(169, 'Anamika Karotoa', 23.8248725, 90.4312027, '<a href=\"https://maps.google.com/maps/contrib/101391725145184873359/photos\">Mahfuz Siddiqui</a>', 'point_of_interest', 3.5, 9),
(170, 'Physio Clinic', 23.8227619, 90.4344038, '', 'hospital', 5, 9),
(171, 'American International University-Bangladesh', 23.8220711, 90.4274569, '<a href=\"https://maps.google.com/maps/contrib/104381660664275810553/photos\">Imam Hasan Rafsun</a>', 'university', 4.7, 9),
(172, 'GeekHouse', 23.8250655, 90.4305589, '', 'lodging', 4.7, 9),
(173, 'FnF Properties Ltd.', 23.8187849, 90.4343904, '', 'general_contractor', 3.5, 9),
(174, 'Silicon Tower', 23.8176659, 90.4292749, '', 'point_of_interest', 4, 9),
(175, 'Ecotech Valencia', 23.8188134, 90.4274323, '', 'point_of_interest', 5, 9),
(176, 'Ferimen', 23.8184281, 90.4277102, '<a href=\"https://maps.google.com/maps/contrib/117019491939928778764/photos\">Hasnain Ahmad Tanim</a>', 'point_of_interest', 2, 9),
(177, 'Bashundhara Convention Center 2', 23.8218308, 90.4290214, '<a href=\"https://maps.google.com/maps/contrib/115675456271849866148/photos\">Ramil Hossain</a>', 'point_of_interest', 3.6, 9),
(178, 'BeeLab Inc.', 23.817246, 90.430092, '<a href=\"https://maps.google.com/maps/contrib/108912449350732939313/photos\">BeeLab Inc.</a>', 'point_of_interest', 0, 9),
(179, 'Rakib Mansion', 23.821158, 90.431078, '', 'point_of_interest', 5, 9),
(180, 'Dcore IT', 23.756639, 90.464391, '<a href=\"https://maps.google.com/maps/contrib/106245357826910170665/photos\">Dcore IT</a>', 'point_of_interest', 0, 10),
(181, 'Best Tutorial Site', 23.7564485, 90.4637539, '', 'point_of_interest', 5, 10),
(182, 'Sojib Store', 23.7598856, 90.4628699, '<a href=\"https://maps.google.com/maps/contrib/109026343867025901817/photos\">Tachlim khan</a>', 'shopping_mall', 1, 10),
(183, 'Aster Infotech Academy', 23.7592221, 90.4614325, '', 'point_of_interest', 0, 10),
(184, 'building technology & ideas ltd', 23.7566389, 90.4643906, '', 'point_of_interest', 0, 10),
(185, 'Help Aid Hospital', 23.7566389, 90.4643906, '', 'hospital', 0, 10),
(186, 'Sharif & Co.', 23.7566389, 90.4643906, '', 'point_of_interest', 0, 10),
(187, 'à¦–à¦¿à¦²à¦—à¦¾à¦à¦“ à¦¸à¦¾à¦‰à¦¥ à¦¸à§‹à¦¸à¦¾à¦‡à¦Ÿà¦¿', 23.7566389, 90.4643906, '', 'local_government_office', 0, 10),
(188, 'Dbl Garden View', 23.7566389, 90.4643906, '', 'point_of_interest', 0, 10),
(189, 'Baitul Huda Ahle Hadish Jame Mosjid, Adorsho Para, Nasirabad', 23.7566389, 90.4643906, '<a href=\"https://maps.google.com/maps/contrib/112138421699458395601/photos\">Md Almin</a>', 'mosque', 5, 10),
(190, 'Tamburabad Jame Mosques', 23.7566389, 90.4643906, '', 'mosque', 0, 10),
(191, 'Delocious', 23.756613, 90.4645633, '<a href=\"https://maps.google.com/maps/contrib/101879137494888295554/photos\">Adi Islam</a>', 'restaurant', 5, 10),
(192, 'Lion Hati Jame Mosjid', 23.7582623, 90.4627454, '', 'mosque', 0, 10),
(193, 'Md. Al Amin Store', 23.7587156, 90.4627041, '', 'food', 4, 10),
(194, 'Matrichaya Ideal School', 23.7587782, 90.4622197, '', 'school', 4, 10),
(195, 'Trimohini Bridge', 23.7598856, 90.4628699, '<a href=\"https://maps.google.com/maps/contrib/104136899779967175595/photos\">Sayed Nayem</a>', 'point_of_interest', 4.5, 10),
(196, 'Trimohoni Bazar, Nasirabad,Khilgaon,Dhaka', 23.7598856, 90.4628699, '<a href=\"https://maps.google.com/maps/contrib/111848556701864103145/photos\">Amir foysal Rayhan</a>', 'real_estate_agency', 4, 10),
(197, 'Jashim Hotel And Restaurant', 23.759568, 90.4619766, '', 'restaurant', 0, 10),
(198, 'Mofazzel General Store', 23.7595456, 90.4619367, '', 'food', 0, 10),
(199, 'icddr,b Field Office', 23.817248, 90.3739053, '<a href=\"https://maps.google.com/maps/contrib/114430649149804926891/photos\">Jewel Rana</a>', 'point_of_interest', 4.3, 11),
(200, 'Karbala, Polash Nogor', 23.8173375, 90.3772141, '<a href=\"https://maps.google.com/maps/contrib/114350135820186889562/photos\">Md Arifur Rahman Khan</a', 'point_of_interest', 4, 11),
(201, 'Progoti High School', 23.8178908, 90.374364, '<a href=\"https://maps.google.com/maps/contrib/112945489260707785149/photos\">Make Money</a>', 'school', 3.9, 11),
(202, 'SoftTechBD Ltd.', 23.819378, 90.374027, '<a href=\"https://maps.google.com/maps/contrib/109066046503336583077/photos\">Md Maruf Adnan Sami</a>', 'point_of_interest', 5, 11),
(203, 'Sinthia Computer', 23.8189165, 90.3754663, '', 'point_of_interest', 0, 11),
(204, 'Mirpur Agrani School', 23.8174507, 90.3721668, '<a href=\"https://maps.google.com/maps/contrib/116401601502952441110/photos\">mr obaidullah</a>', 'school', 5, 11),
(205, 'Rasim Networks', 23.8161836, 90.3744334, '<a href=\"https://maps.google.com/maps/contrib/107697758357091791998/photos\">Raihan Hossain</a>', 'point_of_interest', 5, 11),
(206, 'NCSD Field Office, icddr,b', 23.8208366, 90.374125, '', 'point_of_interest', 0, 11),
(207, 'Pusti Office, icddr,b', 23.8184694, 90.3770775, '', 'point_of_interest', 0, 11),
(208, 'M/s Wasim Sanitary', 23.8187913, 90.3770756, '<a href=\"https://maps.google.com/maps/contrib/109902302829743306844/photos\">Mehedi Hassan Niaz</a>', 'point_of_interest', 4, 11),
(209, 'Best Holidays Ltd', 23.8184675, 90.3771598, '<a href=\"https://maps.google.com/maps/contrib/115497203126457050800/photos\">Haji Anwar Hossain Rony<', 'travel_agency', 5, 11),
(210, 'Smart IT Bangladesh', 23.8184871, 90.3772715, '', 'point_of_interest', 5, 11),
(211, 'Anowara Telecom', 23.8185951, 90.3772715, '<a href=\"https://maps.google.com/maps/contrib/118233999536103384422/photos\">osman goni</a>', 'real_estate_agency', 4.6, 11),
(212, 'BRAC Bank Limited ATM', 23.8213009, 90.3731284, '<a href=\"https://maps.google.com/maps/contrib/103449932324254481203/photos\">Panna Rahman</a>', 'atm', 3.7, 11),
(213, 'Standard Group (à¦¸à§à¦Ÿà§à¦¯à¦¾à¦¨à§à¦¡à¦¾à¦°à§à¦¡ à¦—à§à¦°à§à¦ª)', 23.8216843, 90.3751659, '<a href=\"https://maps.google.com/maps/contrib/101770683121234613834/photos\">Masud Rana</a>', 'point_of_interest', 4.4, 11),
(214, 'Onirban Bangladesh', 23.8214647, 90.3734225, '<a href=\"https://maps.google.com/maps/contrib/105587236503438322394/photos\">Abdullah Al Fahad</a>', 'point_of_interest', 4.8, 11),
(215, 'Reflection', 23.820471, 90.3768786, '', 'point_of_interest', 5, 11),
(216, 'Marie Stopes', 23.8214831, 90.3721243, '<a href=\"https://maps.google.com/maps/contrib/102713173221121478074/photos\">Md Almamun</a>', 'health', 4, 11),
(217, 'Kayes & Associates', 23.820867, 90.376616, '<a href=\"https://maps.google.com/maps/contrib/115107776935620094099/photos\">Kayes &amp; Associates</', 'point_of_interest', 5, 11),
(218, 'Shah Villa', 23.8153017, 90.3758086, '', 'point_of_interest', 0, 11),
(219, 'Marino Hotel', 23.8637594, 90.4057196, '<a href=\"https://maps.google.com/maps/contrib/114946181082638743484/photos\">Forhad Ahmed</a>', 'lodging', 3.9, 12),
(220, 'Comfort Inn', 23.8634845, 90.405506, '<a href=\"https://maps.google.com/maps/contrib/114013494927722129248/photos\">Hotel Comfort Inn</a>', 'lodging', 3.7, 12),
(221, 'Platinum Residence', 23.8678817, 90.4051285, '<a href=\"https://maps.google.com/maps/contrib/111208044373004775719/photos\">emran seu</a>', 'lodging', 4.1, 12),
(222, 'Uttara University', 23.8700347, 90.4027162, '<a href=\"https://maps.google.com/maps/contrib/110426420932591709838/photos\">Jaber Al Nahian</a>', 'university', 4.2, 12),
(223, 'Rajuk Uttara Model College', 23.870234, 90.4017865, '<a href=\"https://maps.google.com/maps/contrib/117700816167558549241/photos\">arafat hossain ayon</a>', 'university', 4.4, 12),
(224, 'Hotel City Homes', 23.8682476, 90.4033149, '<a href=\"https://maps.google.com/maps/contrib/118390420559685830824/photos\">minhaj reza</a>', 'lodging', 3.6, 12),
(225, 'Perdana College of Malaysia', 23.867418, 90.403136, '<a href=\"https://maps.google.com/maps/contrib/101577728705471931774/photos\">Perdana College of Malay', 'university', 5, 12),
(226, 'Hermes Otto International', 23.868126, 90.4016841, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'point_of_interest', 4.8, 12),
(227, 'Teletalk Customer Care Center', 23.8681154, 90.4023666, '<a href=\"https://maps.google.com/maps/contrib/116262399778862319044/photos\">Kanchan Khisa</a>', 'point_of_interest', 3.5, 12),
(228, 'Reed Consulting (BD) Limited', 23.8682328, 90.4036104, '<a href=\"https://maps.google.com/maps/contrib/111254286564699721307/photos\">Reed Consulting (BD) Lim', 'point_of_interest', 4, 12),
(229, 'Li & Fung', 23.8681271, 90.4017922, '', 'point_of_interest', 4, 12),
(230, 'Smartex', 23.8678367, 90.4017946, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'clothing_store', 3.9, 12),
(231, 'Rahimafroz Sales and Service Centre', 23.86825, 90.4036486, '<a href=\"https://maps.google.com/maps/contrib/117782080079491872940/photos\">Creative buzz</a>', 'car_repair', 5, 12),
(232, 'Speed King', 23.8693421, 90.403262, '<a href=\"https://maps.google.com/maps/contrib/113807853514220782667/photos\">Mehedi Hasan</a>', 'point_of_interest', 4.7, 12),
(233, 'TÃœV SÃœD Bangladesh (Pvt.) Ltd', 23.8683818, 90.4018804, '<a href=\"https://maps.google.com/maps/contrib/118188038379193455476/photos\">Update Tower</a>', 'point_of_interest', 4.4, 12),
(234, 'Institute of Mutual Information', 23.865544, 90.401961, '<a href=\"https://maps.google.com/maps/contrib/100637307326754225107/photos\">DIY at Home</a>', 'point_of_interest', 4.8, 12),
(235, 'Trimatik Home Appliance Service Ltd.', 23.866858, 90.404757, '<a href=\"https://maps.google.com/maps/contrib/115844951088828526171/photos\">Trimatik Home Appliance ', 'point_of_interest', 0, 12),
(236, 'Trimatrik Tech', 23.8671342, 90.4046739, '<a href=\"https://maps.google.com/maps/contrib/110777660436596151753/photos\">Trimatrik Tech</a>', 'electronics_store', 4.5, 12),
(237, 'MNK Design Ltd', 23.8671109, 90.4041941, '', 'clothing_store', 4, 12),
(238, 'JC Group', 23.8674908, 90.4046199, '', 'point_of_interest', 0, 12),
(239, 'ATI Limited', 23.8706412, 90.3983815, '<a href=\"https://maps.google.com/maps/contrib/108784688384810130745/photos\">Shaikh Abu Reza</a>', 'point_of_interest', 3.9, 13),
(240, 'Dutch-Bangla Bank Limited ATM', 23.8686041, 90.3936455, '<a href=\"https://maps.google.com/maps/contrib/106898798030706536087/photos\">Mohd. Abdul Awal</a>', 'atm', 3.4, 13),
(241, 'Asian University of Bangladesh', 23.8707991, 90.3997152, '<a href=\"https://maps.google.com/maps/contrib/106800974689211727560/photos\">Afaque Adil</a>', 'university', 4.4, 13),
(242, 'Sa Fashion', 23.8737169, 90.3973371, '', 'home_goods_store', 5, 13),
(243, 'Platinum Home', 23.8695479, 90.3910178, '', 'point_of_interest', 3.8, 13),
(244, 'Trisan Hotel & Resort Ltd', 23.8721717, 90.3989356, '', 'point_of_interest', 3.5, 13),
(245, 'Dutch-Bangla Bank Limited', 23.8745925, 90.3930519, '<a href=\"https://maps.google.com/maps/contrib/115000938235311150637/photos\">Asadujjaman Pappu</a>', 'bank', 4.1, 13),
(246, 'Uttara High School and College', 23.8709006, 90.3972596, '<a href=\"https://maps.google.com/maps/contrib/112453645377626185547/photos\">Abdullah al-mujahid</a>', 'school', 4.3, 13),
(247, 'Budget Hotel', 23.8699309, 90.3938555, '', 'lodging', 0, 13),
(248, 'jemtexltd', 23.8730792, 90.3940541, '', 'finance', 4, 13),
(249, 'Uttara Model School', 23.8716183, 90.3973971, '<a href=\"https://maps.google.com/maps/contrib/106671526848421578828/photos\">Uttara Model School</a>', 'school', 3.7, 13),
(250, 'ElanceIT', 23.871327, 90.39716, '<a href=\"https://maps.google.com/maps/contrib/109695941435495745341/photos\">ElanceIT</a>', 'point_of_interest', 4.7, 13),
(251, 'BRAC Bank Limited ATM', 23.8695522, 90.3935391, '<a href=\"https://maps.google.com/maps/contrib/102616886301050511904/photos\">Asive Chowdhury</a>', 'atm', 4.2, 13),
(252, 'Eastern Bank Limited ATM', 23.8684889, 90.3936168, '<a href=\"https://maps.google.com/maps/contrib/101125321244312960784/photos\">Hamidul Arefin Rizu</a>', 'atm', 4.1, 13),
(253, 'Milkyway Soft', 23.86916, 90.396766, '', 'point_of_interest', 0, 13),
(254, 'Tanjimul Ummah Alim Madrasah', 23.8721374, 90.3978308, '', 'school', 4.2, 13),
(255, 'International Education Centre', 23.870582, 90.395274, '<a href=\"https://maps.google.com/maps/contrib/100147735329150431142/photos\">International Education ', 'school', 4.8, 13),
(256, 'Marketa2z.com', 23.8682669, 90.3960872, '<a href=\"https://maps.google.com/maps/contrib/115136405390023246606/photos\">Marketa2z.com</a>', 'point_of_interest', 0, 13),
(257, 'Euro Tech Bangladesh', 23.8726181, 90.3949982, '', 'point_of_interest', 0, 13),
(258, 'NEW LOOK FABRICS / NEW LOOK International', 23.872694, 90.397169, '', 'point_of_interest', 0, 13),
(259, 'Dutch-Bangla Bank Limited ATM', 23.8647543, 90.39429, '<a href=\"https://maps.google.com/maps/contrib/102577011780154106588/photos\">Uzzal Hossen</a>', 'atm', 3.5, 14),
(260, 'Dutch-Bangla Bank Limited ATM', 23.8686041, 90.3936455, '<a href=\"https://maps.google.com/maps/contrib/106898798030706536087/photos\">Mohd. Abdul Awal</a>', 'atm', 3.4, 14),
(261, 'Platinum Home', 23.8695479, 90.3910178, '', 'point_of_interest', 3.8, 14),
(262, 'Baitul Aman Jame Masjid', 23.8684861, 90.3885656, '<a href=\"https://maps.google.com/maps/contrib/111833917403775119487/photos\">Masbahul Alam</a>', 'mosque', 4.6, 14),
(263, 'VERITAS', 23.86494, 90.390808, '', 'school', 3, 14),
(264, 'Southeast Bank Limited ATM', 23.866001, 90.388791, '<a href=\"https://maps.google.com/maps/contrib/103822217816415740415/photos\">JAHANGIR ALAM</a>', 'atm', 4.3, 14),
(265, 'Eastern Bank Limited ATM', 23.8684889, 90.3936168, '<a href=\"https://maps.google.com/maps/contrib/101125321244312960784/photos\">Hamidul Arefin Rizu</a>', 'atm', 4.1, 14),
(266, 'Soft Tech Innovation Ltd', 23.867285, 90.3905344, '<a href=\"https://maps.google.com/maps/contrib/113336386612459397243/photos\">SM SOFT TECH</a>', 'point_of_interest', 4.7, 14),
(267, 'Western Educare International', 23.8673458, 90.3916692, '', 'school', 0, 14),
(268, 'RsB', 23.866266, 90.391289, '', 'point_of_interest', 3, 14),
(269, 'Sunnyside School', 23.8663787, 90.3912679, '<a href=\"https://maps.google.com/maps/contrib/109268677075934691316/photos\">Akhtar Hossain</a>', 'school', 4.3, 14),
(270, 'Corporate HR Excellence Bangladesh', 23.8660391, 90.3917417, '', 'point_of_interest', 3.7, 14),
(271, 'Nayantti Apparel Ltd', 23.8659747, 90.3917943, '', 'point_of_interest', 5, 14),
(272, 'MAX SECURE LIMITED', 23.8671619, 90.3914312, '', 'point_of_interest', 4.5, 14),
(273, 'Premier Housing Developments Ltd.', 23.8661967, 90.3899744, '', 'general_contractor', 5, 14),
(274, 'pipra', 23.867287, 90.3905162, '<a href=\"https://maps.google.com/maps/contrib/104648351918500207513/photos\">pipra24</a>', 'clothing_store', 5, 14),
(275, 'Emporium Technology Ltd', 23.867287, 90.390516, '<a href=\"https://maps.google.com/maps/contrib/116422710167952458944/photos\">Emporium Technology Ltd<', 'point_of_interest', 4.2, 14),
(276, 'Peace School Dhaka', 23.8654034, 90.3909084, '<a href=\"https://maps.google.com/maps/contrib/108653184278869461995/photos\">Peace School</a>', 'school', 3.7, 14),
(277, 'FunPark Kidsâ€™ Learning Harbor', 23.8668737, 90.3920746, '<a href=\"https://maps.google.com/maps/contrib/100593185257052542162/photos\">FunPark PreSchool</a>', 'school', 4, 14),
(278, 'Hadid Corporation.', 23.8667372, 90.3922288, '', 'point_of_interest', 0, 14),
(279, 'Jagannath University', 23.7083125, 90.4113225, '<a href=\"https://maps.google.com/maps/contrib/112249457162506886033/photos\">Nazmul sagor</a>', 'university', 4.4, 15),
(280, 'muzicmad', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/109604309566295406503/photos\">khan ashik</a>', 'electronics_store', 5, 15),
(281, 'Today BD', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/106597495768526010960/photos\">Niraj Bhusal</a>', 'point_of_interest', 4.2, 15),
(282, 'Aimraj', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/104517801582234610930/photos\">sb sohag</a>', 'electronics_store', 4.5, 15),
(283, 'self', 23.709921, 90.407143, '', 'point_of_interest', 2, 15),
(284, 'Crucial solution', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(285, 'Tour In Bangladesh', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/114710318033605097591/photos\">kazi ponir</a>', 'travel_agency', 4, 15),
(286, 'Job Loader, à¦œà¦¬ à¦²à§‹à¦¡à¦¾à¦°', 23.710336, 90.406794, '', 'point_of_interest', 1, 15),
(287, 'bd Photoshop Tutorial', 23.709921, 90.407143, '', 'point_of_interest', 4, 15),
(288, 'Dhaka Forex', 23.709921, 90.407143, '', 'point_of_interest', 1, 15),
(289, 'Prime Bank Limited', 23.709921, 90.407143, '', 'bank', 4.2, 15),
(290, 'Caltrax Corporation', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(291, 'E Lab', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(292, 'tapan the king', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/104084809530144344534/photos\">sultan mahamud Tapan</a>', 'jewelry_store', 0, 15),
(293, 'Moneybook2u.Com | Social Network and Export-Import Business', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/117516048210588101817/photos\">Moneybook2u.Com | Social', 'point_of_interest', 0, 15),
(294, 'First Security Islami Bank Ltd.', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/103253318584723986290/photos\">mohammad shafaeat ullah ', 'bank', 0, 15),
(295, 'Nano Information Technology (Nanosoft)', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/105215128909175172884/photos\">Nano Information Technol', 'electronics_store', 0, 15),
(296, 'Titan Americas', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(297, 'M.K.Electronics', 23.708548, 90.408865, '<a href=\"https://maps.google.com/maps/contrib/113166481180997459172/photos\">Hasan Ahmed</a>', 'electronics_store', 4.1, 15),
(298, 'PLUS SOLUTION', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/100822038454240835485/photos\">PLUS SOLUTION</a>', 'point_of_interest', 0, 15),
(299, 'Centre For The Rehabilitation Of The Paralysed - Mirpur', 23.8037968, 90.3871225, '<a href=\"https://maps.google.com/maps/contrib/115697856952033413479/photos\">A.b. Parvez</a>', 'hospital', 4.3, 1),
(300, 'Institute of Science Trade & Technology', 23.8077126, 90.3827075, '<a href=\"https://maps.google.com/maps/contrib/112186461677031759856/photos\">Dipongkar Paul</a>', 'university', 4, 1),
(301, 'City Within by Navana', 23.8049815, 90.3833989, '<a href=\"https://maps.google.com/maps/contrib/118350549836891635836/photos\">Md. Hamidur Rahman (Sr. ', 'point_of_interest', 4, 1),
(302, 'Bijoy Rakeen City', 23.8043925, 90.3819808, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 1),
(303, 'Public Order Management', 23.8022285, 90.3824868, '<a href=\"https://maps.google.com/maps/contrib/101621553167652419886/photos\">Robel Akram</a>', 'police', 4, 1),
(304, 'Projonmo Hostel', 23.8079223, 90.3825223, '', 'lodging', 0, 1),
(305, 'Vashantek Govt. College', 23.8076834, 90.3851173, '<a href=\"https://maps.google.com/maps/contrib/114141555719730834669/photos\">Tania Islam</a>', 'university', 4, 1),
(306, 'Joynagar Project, National Housing Authority', 23.8081318, 90.3830328, '<a href=\"https://maps.google.com/maps/contrib/103905036213820156689/photos\">Feroze Ibne Ali</a>', 'point_of_interest', 3.2, 1),
(307, 'Central University of Science & Technology', 23.8039925, 90.3871446, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'university', 3.5, 1),
(308, 'Taskeater', 23.8037566, 90.3871272, '<a href=\"https://maps.google.com/maps/contrib/113608765078733120872/photos\">Aiman Ghazi</a>', 'point_of_interest', 3.5, 1),
(309, 'PSC Convention Hall', 23.8042594, 90.3796452, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 4.2, 1),
(310, 'University of ISTT', 23.8037771, 90.3872385, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'university', 4.3, 1),
(311, 'Police Staff College', 23.802712, 90.3802616, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'police', 4.2, 1),
(312, 'BRP - Bhashantek Rehabilitation Project', 23.8057416, 90.3875471, '<a href=\"https://maps.google.com/maps/contrib/112514969635833713773/photos\">Nazmul Huda</a>', 'lodging', 3.9, 1),
(313, 'Mirpur Cyclists Point', 23.8074742, 90.3803289, '', 'point_of_interest', 5, 1),
(314, 'National Center for Special Education , PHT Center, Jatio Protibondhi Unnoyon Faundation', 23.8030066, 90.3870947, '<a href=\"https://maps.google.com/maps/contrib/102092756546051434002/photos\">Uzzal Mozumder</a>', 'school', 3, 1),
(315, 'Sphinx Corporation', 23.807977, 90.380599, '<a href=\"https://maps.google.com/maps/contrib/109468588753974216819/photos\">Ruhul Quddus</a>', 'point_of_interest', 3.7, 1),
(316, 'Kafrul Police Station', 23.8012549, 90.3814935, '<a href=\"https://maps.google.com/maps/contrib/105505602978119898120/photos\">Abdullah Al Mamun</a>', 'police', 3.7, 1),
(317, 'True Condominum', 23.8053547, 90.3832617, '<a href=\"https://maps.google.com/maps/contrib/100043013517271259287/photos\">Hamid Rahman</a>', 'point_of_interest', 0, 1),
(318, 'RUPOSHI PRO-ACTIVE VILLAGE', 23.805648, 90.384768, '<a href=\"https://maps.google.com/maps/contrib/107669497148670126021/photos\">Waliur Rashid Tamal</a>', 'lodging', 4.6, 1),
(319, 'Pallabi Shopping Centre', 23.8245208, 90.3639328, '<a href=\"https://maps.google.com/maps/contrib/114961232641196714520/photos\">Hafiz Shuputra</a>', 'shopping_mall', 4, 2),
(320, 'Prime Bank Limited ATM', 23.8224486, 90.3641828, '<a href=\"https://maps.google.com/maps/contrib/102404769214802874337/photos\">Young Traveller&#39;s</a', 'atm', 3.7, 2),
(321, 'Pallabi M I Model High School', 23.8257865, 90.3616841, '<a href=\"https://maps.google.com/maps/contrib/104673188384016288051/photos\">Black Hwak</a>', 'school', 4.1, 2),
(322, 'Doulah & Doulah, Law Firm in Dhaka, Bangladesh', 23.824819, 90.364347, '<a href=\"https://maps.google.com/maps/contrib/117931891198332915907/photos\">Doulah &amp; Doulah, Law', 'point_of_interest', 5, 2),
(323, 'California Fried Chicken & Pastry Shop', 23.8235565, 90.3641555, '<a href=\"https://maps.google.com/maps/contrib/102728950613316115424/photos\">Zihad Tarafdar</a>', 'restaurant', 3.8, 2),
(324, 'Cantina, fayon, me&mom, Dabber', 23.8245646, 90.3643586, '<a href=\"https://maps.google.com/maps/contrib/101415674434752194821/photos\">Murshida Ferdous Binte H', 'supermarket', 3, 2),
(325, 'T.K Sports', 23.823772, 90.364485, '<a href=\"https://maps.google.com/maps/contrib/112277507383105396424/photos\">T.K SPORTS</a>', 'store', 3.8, 2),
(326, 'FASHIONEX - Fashion Exclusive Ltd.', 23.824113, 90.36106, '<a href=\"https://maps.google.com/maps/contrib/114985953345657206126/photos\">FASHIONEX - Fashion Excl', 'point_of_interest', 5, 2);
INSERT INTO `poi` (`id`, `name`, `lattitude`, `longitude`, `photo`, `types`, `rating`, `rs_id`) VALUES
(327, 'Renata Limited (à¦°à§‡à¦¨à¦¾à¦Ÿà¦¾ à¦²à¦¿à¦®à¦¿à¦Ÿà§‡à¦¡)', 23.821857, 90.3602703, '<a href=\"https://maps.google.com/maps/contrib/111565435717135097030/photos\">durbar yaqub</a>', 'health', 4.6, 2),
(328, 'Office Automation Technology', 23.8224872, 90.3624904, '', 'point_of_interest', 0, 2),
(329, 'BRAC Bank Limited ATM', 23.824276, 90.361646, '<a href=\"https://maps.google.com/maps/contrib/107107731088201748422/photos\">à¦¶à¦“à¦•à¦¤ à¦®à§à¦°à¦', 'atm', 4.3, 2),
(330, 'X Land', 23.8242693, 90.362168, '<a href=\"https://maps.google.com/maps/contrib/106308026123995557241/photos\">Mahfuz Onty</a>', 'clothing_store', 1, 2),
(331, 'AttireBD', 23.8228205, 90.3624051, '<a href=\"https://maps.google.com/maps/contrib/110395016086196639005/photos\">AttireBD</a>', 'clothing_store', 0, 2),
(332, 'Pallabi Police Fari', 23.82399, 90.3640717, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'police', 3.2, 2),
(333, 'Softbyte IT Solution', 23.824366, 90.361579, '', 'point_of_interest', 5, 2),
(334, 'Infinitech', 23.8242232, 90.3618055, 'Photos are copyrighted by their owners', 'electronics_store', 0, 2),
(335, 'DiaBazar.com', 23.8244986, 90.3619292, '<a href=\"https://maps.google.com/maps/contrib/105612438216352555956/photos\">Shah Md. Sultan</a>', 'shopping_mall', 3.9, 2),
(336, 'WEB TECH SOFT', 23.8245014, 90.3619708, '<a href=\"https://maps.google.com/maps/contrib/115319616129642145710/photos\">green bizz</a>', 'point_of_interest', 5, 2),
(337, 'Moringa Private Limited', 23.8244582, 90.3614102, '<a href=\"https://maps.google.com/maps/contrib/100817643043092274569/photos\">S. M. Arifur Rahman</a>', 'point_of_interest', 0, 2),
(338, 'icddr,b Field Office', 23.817248, 90.3739053, '<a href=\"https://maps.google.com/maps/contrib/114430649149804926891/photos\">Jewel Rana</a>', 'point_of_interest', 4.3, 3),
(339, 'Karbala, Polash Nogor', 23.8173375, 90.3772141, '<a href=\"https://maps.google.com/maps/contrib/114350135820186889562/photos\">Md Arifur Rahman Khan</a', 'point_of_interest', 4, 3),
(340, 'Progoti High School', 23.8178908, 90.374364, '<a href=\"https://maps.google.com/maps/contrib/112945489260707785149/photos\">Make Money</a>', 'school', 3.9, 3),
(341, 'SoftTechBD Ltd.', 23.819378, 90.374027, '<a href=\"https://maps.google.com/maps/contrib/109066046503336583077/photos\">Md Maruf Adnan Sami</a>', 'point_of_interest', 5, 3),
(342, 'Sinthia Computer', 23.8189165, 90.3754663, '', 'point_of_interest', 0, 3),
(343, 'Mirpur Agrani School', 23.8174507, 90.3721668, '<a href=\"https://maps.google.com/maps/contrib/116401601502952441110/photos\">mr obaidullah</a>', 'school', 5, 3),
(344, 'Rasim Networks', 23.8161836, 90.3744334, '<a href=\"https://maps.google.com/maps/contrib/107697758357091791998/photos\">Raihan Hossain</a>', 'point_of_interest', 5, 3),
(345, 'NCSD Field Office, icddr,b', 23.8208366, 90.374125, '', 'point_of_interest', 0, 3),
(346, 'Pusti Office, icddr,b', 23.8184694, 90.3770775, '', 'point_of_interest', 0, 3),
(347, 'M/s Wasim Sanitary', 23.8187913, 90.3770756, '<a href=\"https://maps.google.com/maps/contrib/109902302829743306844/photos\">Mehedi Hassan Niaz</a>', 'point_of_interest', 4, 3),
(348, 'Best Holidays Ltd', 23.8184675, 90.3771598, '<a href=\"https://maps.google.com/maps/contrib/115497203126457050800/photos\">Haji Anwar Hossain Rony<', 'travel_agency', 5, 3),
(349, 'Smart IT Bangladesh', 23.8184871, 90.3772715, '', 'point_of_interest', 5, 3),
(350, 'Anowara Telecom', 23.8185951, 90.3772715, '<a href=\"https://maps.google.com/maps/contrib/118233999536103384422/photos\">osman goni</a>', 'real_estate_agency', 4.6, 3),
(351, 'BRAC Bank Limited ATM', 23.8213009, 90.3731284, '<a href=\"https://maps.google.com/maps/contrib/103449932324254481203/photos\">Panna Rahman</a>', 'atm', 3.7, 3),
(352, 'Standard Group (à¦¸à§à¦Ÿà§à¦¯à¦¾à¦¨à§à¦¡à¦¾à¦°à§à¦¡ à¦—à§à¦°à§à¦ª)', 23.8216843, 90.3751659, '<a href=\"https://maps.google.com/maps/contrib/101770683121234613834/photos\">Masud Rana</a>', 'point_of_interest', 4.4, 3),
(353, 'Onirban Bangladesh', 23.8214647, 90.3734225, '<a href=\"https://maps.google.com/maps/contrib/105587236503438322394/photos\">Abdullah Al Fahad</a>', 'point_of_interest', 4.8, 3),
(354, 'Reflection', 23.820471, 90.3768786, '', 'point_of_interest', 5, 3),
(355, 'Marie Stopes', 23.8214831, 90.3721243, '<a href=\"https://maps.google.com/maps/contrib/102713173221121478074/photos\">Md Almamun</a>', 'health', 4, 3),
(356, 'Kayes & Associates', 23.820867, 90.376616, '<a href=\"https://maps.google.com/maps/contrib/115107776935620094099/photos\">Kayes &amp; Associates</', 'point_of_interest', 5, 3),
(357, 'Shah Villa', 23.8153017, 90.3758086, '', 'point_of_interest', 0, 3),
(358, 'North South University', 23.815103, 90.425538, '<a href=\"https://maps.google.com/maps/contrib/117540630993561226644/photos\">North South University</', 'university', 4.5, 4),
(359, 'GP House', 23.8123408, 90.4259218, '<a href=\"https://maps.google.com/maps/contrib/100812830115690946175/photos\">Sadman_ Sakib016</a>', 'point_of_interest', 4.6, 4),
(360, 'North South University Library', 23.8150956, 90.4269784, '<a href=\"https://maps.google.com/maps/contrib/111678859978078554264/photos\">Mubassir Ahsan</a>', 'library', 4.7, 4),
(361, 'Basic Bank Limited', 23.8128213, 90.4284346, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'bank', 3.8, 4),
(362, 'China inn hotel', 23.816706, 90.426337, '', 'lodging', 0, 4),
(363, 'International Centre for Climate Change and Development', 23.8126119, 90.4300978, '<a href=\"https://maps.google.com/maps/contrib/107415293634291278788/photos\">International Centre for', 'point_of_interest', 4.2, 4),
(364, 'Thinker', 23.811407, 90.425891, '', 'point_of_interest', 2.5, 4),
(365, 'Bashundhara Corporate Office', 23.8118863, 90.4301218, '<a href=\"https://maps.google.com/maps/contrib/116719755110696390888/photos\">Mahmudul Hasan</a>', 'point_of_interest', 4.1, 4),
(366, 'Grameenphone Limited', 23.8120617, 90.4259954, '<a href=\"https://maps.google.com/maps/contrib/104556028239764196853/photos\">Hasib Rezu</a>', 'point_of_interest', 4.3, 4),
(367, 'North South University Auditorium', 23.8156336, 90.4270064, '<a href=\"https://maps.google.com/maps/contrib/106036129065840886710/photos\">Mohammad Abu Sayed</a>', 'point_of_interest', 4.5, 4),
(368, 'Genetic Mayabee', 23.8135871, 90.4290524, '<a href=\"https://maps.google.com/maps/contrib/109564497076452734030/photos\">Genetic Mayabee</a>', 'point_of_interest', 3.7, 4),
(369, 'Social Islami Bank Limited', 23.8128241, 90.4289117, '<a href=\"https://maps.google.com/maps/contrib/118292777726534178700/photos\">Syed Shafqat Rabbi</a>', 'bank', 4.3, 4),
(370, 'Southeast Bank Limited', 23.8137797, 90.4284443, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'bank', 3.8, 4),
(371, 'Eastern Bank Limited', 23.8128311, 90.4286653, '<a href=\"https://maps.google.com/maps/contrib/105655298857197313292/photos\">Ahnaf Brinto</a>', 'bank', 4.5, 4),
(372, 'Prime Bank Limited, Bosundhara Branch.', 23.8129421, 90.4269899, '<a href=\"https://maps.google.com/maps/contrib/101050510210647412274/photos\">SOHEL talukder</a>', 'bank', 4.2, 4),
(373, 'Mutual Trust Bank Limited', 23.812919, 90.4269346, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'bank', 4.3, 4),
(374, 'BRAC Bank Limited ATM', 23.8136629, 90.4282039, '<a href=\"https://maps.google.com/maps/contrib/117989094771985852080/photos\">Momenul Islam</a>', 'atm', 4, 4),
(375, 'Eastern Bank Limited ATM', 23.8128197, 90.4288558, '<a href=\"https://maps.google.com/maps/contrib/104556028239764196853/photos\">Hasib Rezu</a>', 'atm', 4.5, 4),
(376, 'GadgetGang7', 23.8128442, 90.4246735, '<a href=\"https://maps.google.com/maps/contrib/108138387223724130038/photos\">GadgetGang7</a>', 'electronics_store', 3.5, 4),
(377, 'Movenpick', 23.8126893, 90.4282117, '<a href=\"https://maps.google.com/maps/contrib/106198848214928804762/photos\">Foysal Ahmed</a>', 'food', 4.3, 4),
(378, 'Police Staff College', 23.802712, 90.3802616, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'police', 4.2, 5),
(379, 'Kafrul Police Station', 23.8012549, 90.3814935, '<a href=\"https://maps.google.com/maps/contrib/105505602978119898120/photos\">Abdullah Al Mamun</a>', 'police', 3.7, 5),
(380, 'Public Order Management', 23.8022285, 90.3824868, '<a href=\"https://maps.google.com/maps/contrib/101621553167652419886/photos\">Robel Akram</a>', 'police', 4, 5),
(381, 'Government Unani and Ayurvedic Medical College', 23.8040938, 90.3781197, '<a href=\"https://maps.google.com/maps/contrib/111794091994874898657/photos\">aliuzzaman tushar</a>', 'hospital', 3.9, 5),
(382, 'Institute of Science Trade & Technology', 23.8077126, 90.3827075, '<a href=\"https://maps.google.com/maps/contrib/112186461677031759856/photos\">Dipongkar Paul</a>', 'university', 4, 5),
(383, 'Bijoy Rakeen City', 23.8043925, 90.3819808, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 5),
(384, 'City Within by Navana', 23.8049815, 90.3833989, '<a href=\"https://maps.google.com/maps/contrib/118350549836891635836/photos\">Md. Hamidur Rahman (Sr. ', 'point_of_interest', 4, 5),
(385, 'PSC Convention Hall', 23.8042594, 90.3796452, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 4.2, 5),
(386, 'Bangladesh Krishi Bank Training Institute', 23.8035661, 90.3784838, '<a href=\"https://maps.google.com/maps/contrib/118191667416027851236/photos\">Hemanta Biswas</a>', 'bank', 3.7, 5),
(387, 'Team Travel', 23.8031672, 90.3786645, '', 'point_of_interest', 0, 5),
(388, 'President Zone', 23.8041082, 90.3782257, '<a href=\"https://maps.google.com/maps/contrib/110837407459038494424/photos\">à¦¬à¦¿à¦¨à§‹à¦¦à¦¨ à¦¬à¦', 'point_of_interest', 0, 5),
(389, 'Mirpur Cyclists Point', 23.8074742, 90.3803289, '', 'point_of_interest', 5, 5),
(390, 'Sphinx Corporation', 23.807977, 90.380599, '<a href=\"https://maps.google.com/maps/contrib/109468588753974216819/photos\">Ruhul Quddus</a>', 'point_of_interest', 3.7, 5),
(391, 'MBM Garments Ltd', 23.8025934, 90.3784319, '', 'store', 4, 5),
(392, 'Hatekhori web ltd.', 23.802168, 90.378558, '', 'point_of_interest', 0, 5),
(393, 'Projonmo Hostel', 23.8079223, 90.3825223, '', 'lodging', 0, 5),
(394, 'So Cheap Hosting', 23.802029, 90.378663, '<a href=\"https://maps.google.com/maps/contrib/105750972030885545910/photos\">So Cheap Hosting</a>', 'point_of_interest', 5, 5),
(395, 'Rishal Group of Industries', 23.8034186, 90.3777931, '', 'point_of_interest', 4, 5),
(396, 'Kalni', 23.8021271, 90.3785405, '<a href=\"https://maps.google.com/maps/contrib/105440891601708067996/photos\">Kalni</a>', 'point_of_interest', 3.6, 5),
(397, 'sknews24.com', 23.802131, 90.378493, '<a href=\"https://maps.google.com/maps/contrib/113763818822976623921/photos\">sknews24.com</a>', 'point_of_interest', 5, 5),
(398, 'Marino Hotel', 23.8637594, 90.4057196, '<a href=\"https://maps.google.com/maps/contrib/114946181082638743484/photos\">Forhad Ahmed</a>', 'lodging', 3.9, 6),
(399, 'Richmond Hotel & Suites', 23.8592696, 90.4005698, '<a href=\"https://maps.google.com/maps/contrib/110503991145201710871/photos\">MD. MAHABUB ALAM SAYED</', 'lodging', 3.8, 6),
(400, 'Comfort Inn', 23.8634845, 90.405506, '<a href=\"https://maps.google.com/maps/contrib/114013494927722129248/photos\">Hotel Comfort Inn</a>', 'lodging', 3.7, 6),
(401, 'Dutch Bangla Bank Limited ATM', 23.8608119, 90.4006274, '<a href=\"https://maps.google.com/maps/contrib/118081416268371263829/photos\">Maidul Islam</a>', 'atm', 4.2, 6),
(402, 'SYL Valley Inn', 23.8623278, 90.398998, '', 'lodging', 0, 6),
(403, 'Pizza Inn', 23.8609509, 90.3992559, '<a href=\"https://maps.google.com/maps/contrib/110076743361396152463/photos\">Ismail Bablu</a>', 'restaurant', 4.1, 6),
(404, 'Islami Bank Bangladesh Limited', 23.8649261, 90.3996706, '<a href=\"https://maps.google.com/maps/contrib/107338546288245819609/photos\">Kazi Muhshin</a>', 'bank', 4.1, 6),
(405, 'MaxRenda Apparels & Accessories Ltd.', 23.8641395, 90.3995238, '', 'point_of_interest', 0, 6),
(406, 'Uttara Bank Limited, Uttara Branch', 23.8628377, 90.3995596, '<a href=\"https://maps.google.com/maps/contrib/101724367888459891887/photos\">Mirza Khalid</a>', 'bank', 4, 6),
(407, 'The Aga Khan School', 23.8608355, 90.4024108, '<a href=\"https://maps.google.com/maps/contrib/109499377436072467906/photos\">Ahsan Zaman</a>', 'school', 4.3, 6),
(408, 'Aarong Flagship Outlet', 23.8617894, 90.3995311, '<a href=\"https://maps.google.com/maps/contrib/112115021996799214152/photos\">Zahid zico</a>', 'shopping_mall', 4.3, 6),
(409, 'Coffee World Uttara', 23.8609937, 90.3992308, '<a href=\"https://maps.google.com/maps/contrib/108437545261585998040/photos\">Habib Ullah Bahar</a>', 'cafe', 4, 6),
(410, 'DHL Uttara Service Point', 23.8654952, 90.400435, '<a href=\"https://maps.google.com/maps/contrib/117982693800395153651/photos\">Quaribul Hasan.</a>', 'point_of_interest', 4, 6),
(411, 'Berger Paints Bangladesh Limited', 23.8616624, 90.3990872, '<a href=\"https://maps.google.com/maps/contrib/103993600667129623163/photos\">CERULEAN Antar</a>', 'home_goods_store', 4.2, 6),
(412, 'Ankur ICT Development Foundation', 23.8642148, 90.4009688, '', 'point_of_interest', 4.3, 6),
(413, 'The Aga Khan School', 23.8626445, 90.4025419, '<a href=\"https://maps.google.com/maps/contrib/100958826840571883461/photos\">Sayeef Rahman</a>', 'school', 4.1, 6),
(414, 'ANNEX Travel & Consulting Group (Travel Agent- Tour Operator)', 23.8632817, 90.4003008, '<a href=\"https://maps.google.com/maps/contrib/104554642820700009497/photos\">à¦…à§à¦¯à¦¾à¦¨à§‡à¦•à§', 'travel_agency', 5, 6),
(415, 'Mutual Trust Bank Limited', 23.8617511, 90.4003042, '<a href=\"https://maps.google.com/maps/contrib/105612438216352555956/photos\">Shah Md. Sultan</a>', 'bank', 3.5, 6),
(416, 'United College of Aviation Science and Management', 23.8627185, 90.3982705, '<a href=\"https://maps.google.com/maps/contrib/107712329997587398529/photos\">Imran&#39;s Creation</a>', 'university', 4.1, 6),
(417, 'Bank Asia Limited', 23.8625676, 90.4002862, '<a href=\"https://maps.google.com/maps/contrib/103449932324254481203/photos\">Panna Rahman</a>', 'bank', 3.9, 6),
(418, 'Jinghua Bangla Tours & Travels Ltd', 23.794008, 90.413934, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'travel_agency', 4, 7),
(419, 'Embassy of Spain', 23.7940218, 90.4125499, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'embassy', 4.1, 7),
(420, 'Bata', 23.7938262, 90.4145431, '<a href=\"https://maps.google.com/maps/contrib/110290886265328164855/photos\">Syed Naved Abdullah</a>', 'shoe_store', 4.3, 7),
(421, 'The Westin Dhaka', 23.7931842, 90.4146906, '<a href=\"https://maps.google.com/maps/contrib/112730737981224409177/photos\">The Westin Dhaka</a>', 'lodging', 4.5, 7),
(422, 'Quality Inn', 23.7967872, 90.4106368, '<a href=\"https://maps.google.com/maps/contrib/102109233717354650317/photos\">Biplob Biplob</a>', 'lodging', 3.7, 7),
(423, 'Premier Bank Limited', 23.79308, 90.4155004, '<a href=\"https://maps.google.com/maps/contrib/100184291643871645549/photos\">Zeenat Islam</a>', 'bank', 4.1, 7),
(424, 'Gulshan 2 Kacha Bazar', 23.7945681, 90.4154983, '<a href=\"https://maps.google.com/maps/contrib/110874094855255188159/photos\">Shahzad ali Khurram</a>', 'point_of_interest', 3.8, 7),
(425, 'Colonial Residence (Park View)', 23.7968778, 90.4156777, '<a href=\"https://maps.google.com/maps/contrib/101126202178614772564/photos\">oranm amjuk</a>', 'lodging', 4, 7),
(426, 'Australian High Commission', 23.8001053, 90.4129599, '<a href=\"https://maps.google.com/maps/contrib/107260561261604967583/photos\">Dark Circle</a>', 'embassy', 4.2, 7),
(427, 'Embassy of the Federal Republic of Germany', 23.798818, 90.4129237, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'embassy', 4.2, 7),
(428, 'Embassy of the Netherlands', 23.7965535, 90.4149598, '<a href=\"https://maps.google.com/maps/contrib/111957403240538926778/photos\">Ahmed Galeb</a>', 'embassy', 4.4, 7),
(429, 'Gulshan Circle-2, Dhaka.', 23.7948574, 90.4142119, '<a href=\"https://maps.google.com/maps/contrib/100516462078489469127/photos\">MD. MUSFIQUR RAHMAN</a>', 'neighborhood', 4.2, 7),
(430, 'IDP Education Bangladesh Pvt Ltd', 23.7951463, 90.4147704, '<a href=\"https://maps.google.com/maps/contrib/105191075201109407407/photos\">IDP Education Bangladesh', 'point_of_interest', 3.7, 7),
(431, 'Embassy of Sweden', 23.7981787, 90.4122173, '<a href=\"https://maps.google.com/maps/contrib/107718361547306758571/photos\">MD.SHAHAB UDDIN</a>', 'embassy', 4.3, 7),
(432, 'Maple International Ltd', 23.7968315, 90.4118002, '', 'point_of_interest', 0, 7),
(433, 'Khazana', 23.7955844, 90.4119306, '<a href=\"https://maps.google.com/maps/contrib/112580956857541586694/photos\">Prabhakar Mishra</a>', 'restaurant', 4.1, 7),
(434, 'Doreen Tower Heliport', 23.7944589, 90.41383, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'airport', 3.9, 7),
(435, 'Cambrian College', 23.794173, 90.41424, '<a href=\"https://maps.google.com/maps/contrib/110874094855255188159/photos\">Shahzad ali Khurram</a>', 'university', 3.5, 7),
(436, 'Australian International School', 23.7999282, 90.4139868, '<a href=\"https://maps.google.com/maps/contrib/105768614470244482024/photos\">Aref Farook</a>', 'school', 4.3, 7),
(437, 'Unimart', 23.7959476, 90.4147721, '<a href=\"https://maps.google.com/maps/contrib/104592893675742187537/photos\">aminul Islam</a>', 'supermarket', 4.4, 7),
(438, 'Police Staff College', 23.802712, 90.3802616, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'police', 4.2, 8),
(439, 'Kafrul Police Station', 23.8012549, 90.3814935, '<a href=\"https://maps.google.com/maps/contrib/105505602978119898120/photos\">Abdullah Al Mamun</a>', 'police', 3.7, 8),
(440, 'Public Order Management', 23.8022285, 90.3824868, '<a href=\"https://maps.google.com/maps/contrib/101621553167652419886/photos\">Robel Akram</a>', 'police', 4, 8),
(441, 'Government Unani and Ayurvedic Medical College', 23.8040938, 90.3781197, '<a href=\"https://maps.google.com/maps/contrib/111794091994874898657/photos\">aliuzzaman tushar</a>', 'hospital', 3.9, 8),
(442, 'Institute of Science Trade & Technology', 23.8077126, 90.3827075, '<a href=\"https://maps.google.com/maps/contrib/112186461677031759856/photos\">Dipongkar Paul</a>', 'university', 4, 8),
(443, 'Bijoy Rakeen City', 23.8043925, 90.3819808, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 8),
(444, 'City Within by Navana', 23.8049815, 90.3833989, '<a href=\"https://maps.google.com/maps/contrib/118350549836891635836/photos\">Md. Hamidur Rahman (Sr. ', 'point_of_interest', 4, 8),
(445, 'PSC Convention Hall', 23.8042594, 90.3796452, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 4.2, 8),
(446, 'Bangladesh Krishi Bank Training Institute', 23.8035661, 90.3784838, '<a href=\"https://maps.google.com/maps/contrib/118191667416027851236/photos\">Hemanta Biswas</a>', 'bank', 3.7, 8),
(447, 'Team Travel', 23.8031672, 90.3786645, '', 'point_of_interest', 0, 8),
(448, 'President Zone', 23.8041082, 90.3782257, '<a href=\"https://maps.google.com/maps/contrib/110837407459038494424/photos\">à¦¬à¦¿à¦¨à§‹à¦¦à¦¨ à¦¬à¦', 'point_of_interest', 0, 8),
(449, 'Mirpur Cyclists Point', 23.8074742, 90.3803289, '', 'point_of_interest', 5, 8),
(450, 'Sphinx Corporation', 23.807977, 90.380599, '<a href=\"https://maps.google.com/maps/contrib/109468588753974216819/photos\">Ruhul Quddus</a>', 'point_of_interest', 3.7, 8),
(451, 'MBM Garments Ltd', 23.8025934, 90.3784319, '', 'store', 4, 8),
(452, 'Hatekhori web ltd.', 23.802168, 90.378558, '', 'point_of_interest', 0, 8),
(453, 'Projonmo Hostel', 23.8079223, 90.3825223, '', 'lodging', 0, 8),
(454, 'So Cheap Hosting', 23.802029, 90.378663, '<a href=\"https://maps.google.com/maps/contrib/105750972030885545910/photos\">So Cheap Hosting</a>', 'point_of_interest', 5, 8),
(455, 'Rishal Group of Industries', 23.8034186, 90.3777931, '', 'point_of_interest', 4, 8),
(456, 'Kalni', 23.8021271, 90.3785405, '<a href=\"https://maps.google.com/maps/contrib/105440891601708067996/photos\">Kalni</a>', 'point_of_interest', 3.6, 8),
(457, 'sknews24.com', 23.802131, 90.378493, '<a href=\"https://maps.google.com/maps/contrib/113763818822976623921/photos\">sknews24.com</a>', 'point_of_interest', 5, 8),
(458, 'Viquarunnisa Noon School and College Bashundhara Branch', 23.8205979, 90.4317413, '<a href=\"https://maps.google.com/maps/contrib/110660822343565480525/photos\">Md. Muzahid Bin-Mizan</a', 'school', 4.3, 9),
(459, 'HSBC Bank Limited ATM', 23.8220106, 90.4293195, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'atm', 4.8, 9),
(460, 'SnapShot', 23.822334, 90.433389, '<a href=\"https://maps.google.com/maps/contrib/110705391895031412986/photos\">SnapShot</a>', 'point_of_interest', 5, 9),
(461, 'Eastern Bank Limited ATM', 23.821596, 90.4293828, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'atm', 4.8, 9),
(462, 'SLOAN SQUARE', 23.8201713, 90.4318145, '<a href=\"https://maps.google.com/maps/contrib/105655298857197313292/photos\">Ahnaf Brinto</a>', 'point_of_interest', 5, 9),
(463, 'HSBC ATM Bashundhara', 23.8220168, 90.4293254, '<a href=\"https://maps.google.com/maps/contrib/110290886265328164855/photos\">Syed Naved Abdullah</a>', 'atm', 4.3, 9),
(464, 'Orange Visuals', 23.8228947, 90.4328986, '<a href=\"https://maps.google.com/maps/contrib/117026954003723073692/photos\">Orange Visuals</a>', 'point_of_interest', 0, 9),
(465, 'Greenland Kaniz Evelina', 23.8193207, 90.4294045, '<a href=\"https://maps.google.com/maps/contrib/108448402556243933986/photos\">Mmjsumit</a>', 'point_of_interest', 0, 9),
(466, 'Greenland Raihan Castle', 23.81859, 90.4318483, '<a href=\"https://maps.google.com/maps/contrib/113373166723905912626/photos\">Amirul Islam</a>', 'point_of_interest', 0, 9),
(467, 'Anamika Karotoa', 23.8248725, 90.4312027, '<a href=\"https://maps.google.com/maps/contrib/101391725145184873359/photos\">Mahfuz Siddiqui</a>', 'point_of_interest', 3.5, 9),
(468, 'Physio Clinic', 23.8227619, 90.4344038, '', 'hospital', 5, 9),
(469, 'American International University-Bangladesh', 23.8220711, 90.4274569, '<a href=\"https://maps.google.com/maps/contrib/104381660664275810553/photos\">Imam Hasan Rafsun</a>', 'university', 4.7, 9),
(470, 'GeekHouse', 23.8250655, 90.4305589, '', 'lodging', 4.7, 9),
(471, 'FnF Properties Ltd.', 23.8187849, 90.4343904, '', 'general_contractor', 3.5, 9),
(472, 'Silicon Tower', 23.8176659, 90.4292749, '', 'point_of_interest', 4, 9),
(473, 'Ecotech Valencia', 23.8188134, 90.4274323, '', 'point_of_interest', 5, 9),
(474, 'Ferimen', 23.8184281, 90.4277102, '<a href=\"https://maps.google.com/maps/contrib/117019491939928778764/photos\">Hasnain Ahmad Tanim</a>', 'point_of_interest', 2, 9),
(475, 'Bashundhara Convention Center 2', 23.8218308, 90.4290214, '<a href=\"https://maps.google.com/maps/contrib/115675456271849866148/photos\">Ramil Hossain</a>', 'point_of_interest', 3.6, 9),
(476, 'BeeLab Inc.', 23.817246, 90.430092, '<a href=\"https://maps.google.com/maps/contrib/108912449350732939313/photos\">BeeLab Inc.</a>', 'point_of_interest', 0, 9),
(477, 'Rakib Mansion', 23.821158, 90.431078, '', 'point_of_interest', 5, 9),
(478, 'Dcore IT', 23.756639, 90.464391, '<a href=\"https://maps.google.com/maps/contrib/106245357826910170665/photos\">Dcore IT</a>', 'point_of_interest', 0, 10),
(479, 'Best Tutorial Site', 23.7564485, 90.4637539, '', 'point_of_interest', 5, 10),
(480, 'Sojib Store', 23.7598856, 90.4628699, '<a href=\"https://maps.google.com/maps/contrib/109026343867025901817/photos\">Tachlim khan</a>', 'shopping_mall', 1, 10),
(481, 'Aster Infotech Academy', 23.7592221, 90.4614325, '', 'point_of_interest', 0, 10),
(482, 'building technology & ideas ltd', 23.7566389, 90.4643906, '', 'point_of_interest', 0, 10),
(483, 'Help Aid Hospital', 23.7566389, 90.4643906, '', 'hospital', 0, 10),
(484, 'Sharif & Co.', 23.7566389, 90.4643906, '', 'point_of_interest', 0, 10),
(485, 'à¦–à¦¿à¦²à¦—à¦¾à¦à¦“ à¦¸à¦¾à¦‰à¦¥ à¦¸à§‹à¦¸à¦¾à¦‡à¦Ÿà¦¿', 23.7566389, 90.4643906, '', 'local_government_office', 0, 10),
(486, 'Dbl Garden View', 23.7566389, 90.4643906, '', 'point_of_interest', 0, 10),
(487, 'Baitul Huda Ahle Hadish Jame Mosjid, Adorsho Para, Nasirabad', 23.7566389, 90.4643906, '<a href=\"https://maps.google.com/maps/contrib/112138421699458395601/photos\">Md Almin</a>', 'mosque', 5, 10),
(488, 'Tamburabad Jame Mosques', 23.7566389, 90.4643906, '', 'mosque', 0, 10),
(489, 'Delocious', 23.756613, 90.4645633, '<a href=\"https://maps.google.com/maps/contrib/101879137494888295554/photos\">Adi Islam</a>', 'restaurant', 5, 10),
(490, 'Lion Hati Jame Mosjid', 23.7582623, 90.4627454, '', 'mosque', 0, 10),
(491, 'Md. Al Amin Store', 23.7587156, 90.4627041, '', 'food', 4, 10),
(492, 'Matrichaya Ideal School', 23.7587782, 90.4622197, '', 'school', 4, 10),
(493, 'Trimohini Bridge', 23.7598856, 90.4628699, '<a href=\"https://maps.google.com/maps/contrib/104136899779967175595/photos\">Sayed Nayem</a>', 'point_of_interest', 4.5, 10),
(494, 'Trimohoni Bazar, Nasirabad,Khilgaon,Dhaka', 23.7598856, 90.4628699, '<a href=\"https://maps.google.com/maps/contrib/111848556701864103145/photos\">Amir foysal Rayhan</a>', 'real_estate_agency', 4, 10),
(495, 'Jashim Hotel And Restaurant', 23.759568, 90.4619766, '', 'restaurant', 0, 10),
(496, 'Mofazzel General Store', 23.7595456, 90.4619367, '', 'food', 0, 10),
(497, 'icddr,b Field Office', 23.817248, 90.3739053, '<a href=\"https://maps.google.com/maps/contrib/114430649149804926891/photos\">Jewel Rana</a>', 'point_of_interest', 4.3, 11),
(498, 'Karbala, Polash Nogor', 23.8173375, 90.3772141, '<a href=\"https://maps.google.com/maps/contrib/114350135820186889562/photos\">Md Arifur Rahman Khan</a', 'point_of_interest', 4, 11),
(499, 'Progoti High School', 23.8178908, 90.374364, '<a href=\"https://maps.google.com/maps/contrib/112945489260707785149/photos\">Make Money</a>', 'school', 3.9, 11),
(500, 'SoftTechBD Ltd.', 23.819378, 90.374027, '<a href=\"https://maps.google.com/maps/contrib/109066046503336583077/photos\">Md Maruf Adnan Sami</a>', 'point_of_interest', 5, 11),
(501, 'Sinthia Computer', 23.8189165, 90.3754663, '', 'point_of_interest', 0, 11),
(502, 'Mirpur Agrani School', 23.8174507, 90.3721668, '<a href=\"https://maps.google.com/maps/contrib/116401601502952441110/photos\">mr obaidullah</a>', 'school', 5, 11),
(503, 'Rasim Networks', 23.8161836, 90.3744334, '<a href=\"https://maps.google.com/maps/contrib/107697758357091791998/photos\">Raihan Hossain</a>', 'point_of_interest', 5, 11),
(504, 'NCSD Field Office, icddr,b', 23.8208366, 90.374125, '', 'point_of_interest', 0, 11),
(505, 'Pusti Office, icddr,b', 23.8184694, 90.3770775, '', 'point_of_interest', 0, 11),
(506, 'M/s Wasim Sanitary', 23.8187913, 90.3770756, '<a href=\"https://maps.google.com/maps/contrib/109902302829743306844/photos\">Mehedi Hassan Niaz</a>', 'point_of_interest', 4, 11),
(507, 'Best Holidays Ltd', 23.8184675, 90.3771598, '<a href=\"https://maps.google.com/maps/contrib/115497203126457050800/photos\">Haji Anwar Hossain Rony<', 'travel_agency', 5, 11),
(508, 'Smart IT Bangladesh', 23.8184871, 90.3772715, '', 'point_of_interest', 5, 11),
(509, 'Anowara Telecom', 23.8185951, 90.3772715, '<a href=\"https://maps.google.com/maps/contrib/118233999536103384422/photos\">osman goni</a>', 'real_estate_agency', 4.6, 11),
(510, 'BRAC Bank Limited ATM', 23.8213009, 90.3731284, '<a href=\"https://maps.google.com/maps/contrib/103449932324254481203/photos\">Panna Rahman</a>', 'atm', 3.7, 11),
(511, 'Standard Group (à¦¸à§à¦Ÿà§à¦¯à¦¾à¦¨à§à¦¡à¦¾à¦°à§à¦¡ à¦—à§à¦°à§à¦ª)', 23.8216843, 90.3751659, '<a href=\"https://maps.google.com/maps/contrib/101770683121234613834/photos\">Masud Rana</a>', 'point_of_interest', 4.4, 11),
(512, 'Onirban Bangladesh', 23.8214647, 90.3734225, '<a href=\"https://maps.google.com/maps/contrib/105587236503438322394/photos\">Abdullah Al Fahad</a>', 'point_of_interest', 4.8, 11),
(513, 'Reflection', 23.820471, 90.3768786, '', 'point_of_interest', 5, 11),
(514, 'Marie Stopes', 23.8214831, 90.3721243, '<a href=\"https://maps.google.com/maps/contrib/102713173221121478074/photos\">Md Almamun</a>', 'health', 4, 11),
(515, 'Kayes & Associates', 23.820867, 90.376616, '<a href=\"https://maps.google.com/maps/contrib/115107776935620094099/photos\">Kayes &amp; Associates</', 'point_of_interest', 5, 11),
(516, 'Shah Villa', 23.8153017, 90.3758086, '', 'point_of_interest', 0, 11),
(517, 'Marino Hotel', 23.8637594, 90.4057196, '<a href=\"https://maps.google.com/maps/contrib/114946181082638743484/photos\">Forhad Ahmed</a>', 'lodging', 3.9, 12),
(518, 'Comfort Inn', 23.8634845, 90.405506, '<a href=\"https://maps.google.com/maps/contrib/114013494927722129248/photos\">Hotel Comfort Inn</a>', 'lodging', 3.7, 12),
(519, 'Platinum Residence', 23.8678817, 90.4051285, '<a href=\"https://maps.google.com/maps/contrib/111208044373004775719/photos\">emran seu</a>', 'lodging', 4.1, 12),
(520, 'Uttara University', 23.8700347, 90.4027162, '<a href=\"https://maps.google.com/maps/contrib/110426420932591709838/photos\">Jaber Al Nahian</a>', 'university', 4.2, 12),
(521, 'Rajuk Uttara Model College', 23.870234, 90.4017865, '<a href=\"https://maps.google.com/maps/contrib/117700816167558549241/photos\">arafat hossain ayon</a>', 'university', 4.4, 12),
(522, 'Hotel City Homes', 23.8682476, 90.4033149, '<a href=\"https://maps.google.com/maps/contrib/118390420559685830824/photos\">minhaj reza</a>', 'lodging', 3.6, 12),
(523, 'Perdana College of Malaysia', 23.867418, 90.403136, '<a href=\"https://maps.google.com/maps/contrib/101577728705471931774/photos\">Perdana College of Malay', 'university', 5, 12),
(524, 'Hermes Otto International', 23.868126, 90.4016841, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'point_of_interest', 4.8, 12),
(525, 'Teletalk Customer Care Center', 23.8681154, 90.4023666, '<a href=\"https://maps.google.com/maps/contrib/116262399778862319044/photos\">Kanchan Khisa</a>', 'point_of_interest', 3.5, 12),
(526, 'Reed Consulting (BD) Limited', 23.8682328, 90.4036104, '<a href=\"https://maps.google.com/maps/contrib/111254286564699721307/photos\">Reed Consulting (BD) Lim', 'point_of_interest', 4, 12),
(527, 'Li & Fung', 23.8681271, 90.4017922, '', 'point_of_interest', 4, 12),
(528, 'Smartex', 23.8678367, 90.4017946, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'clothing_store', 3.9, 12),
(529, 'Rahimafroz Sales and Service Centre', 23.86825, 90.4036486, '<a href=\"https://maps.google.com/maps/contrib/117782080079491872940/photos\">Creative buzz</a>', 'car_repair', 5, 12),
(530, 'Speed King', 23.8693421, 90.403262, '<a href=\"https://maps.google.com/maps/contrib/113807853514220782667/photos\">Mehedi Hasan</a>', 'point_of_interest', 4.7, 12),
(531, 'TÃœV SÃœD Bangladesh (Pvt.) Ltd', 23.8683818, 90.4018804, '<a href=\"https://maps.google.com/maps/contrib/118188038379193455476/photos\">Update Tower</a>', 'point_of_interest', 4.4, 12),
(532, 'Institute of Mutual Information', 23.865544, 90.401961, '<a href=\"https://maps.google.com/maps/contrib/100637307326754225107/photos\">DIY at Home</a>', 'point_of_interest', 4.8, 12),
(533, 'Trimatik Home Appliance Service Ltd.', 23.866858, 90.404757, '<a href=\"https://maps.google.com/maps/contrib/115844951088828526171/photos\">Trimatik Home Appliance ', 'point_of_interest', 0, 12),
(534, 'Trimatrik Tech', 23.8671342, 90.4046739, '<a href=\"https://maps.google.com/maps/contrib/110777660436596151753/photos\">Trimatrik Tech</a>', 'electronics_store', 4.5, 12),
(535, 'MNK Design Ltd', 23.8671109, 90.4041941, '', 'clothing_store', 4, 12),
(536, 'JC Group', 23.8674908, 90.4046199, '', 'point_of_interest', 0, 12),
(537, 'ATI Limited', 23.8706412, 90.3983815, '<a href=\"https://maps.google.com/maps/contrib/108784688384810130745/photos\">Shaikh Abu Reza</a>', 'point_of_interest', 3.9, 13),
(538, 'Dutch-Bangla Bank Limited ATM', 23.8686041, 90.3936455, '<a href=\"https://maps.google.com/maps/contrib/106898798030706536087/photos\">Mohd. Abdul Awal</a>', 'atm', 3.4, 13),
(539, 'Asian University of Bangladesh', 23.8707991, 90.3997152, '<a href=\"https://maps.google.com/maps/contrib/106800974689211727560/photos\">Afaque Adil</a>', 'university', 4.4, 13),
(540, 'Sa Fashion', 23.8737169, 90.3973371, '', 'home_goods_store', 5, 13),
(541, 'Platinum Home', 23.8695479, 90.3910178, '', 'point_of_interest', 3.8, 13),
(542, 'Trisan Hotel & Resort Ltd', 23.8721717, 90.3989356, '', 'point_of_interest', 3.5, 13),
(543, 'Dutch-Bangla Bank Limited', 23.8745925, 90.3930519, '<a href=\"https://maps.google.com/maps/contrib/115000938235311150637/photos\">Asadujjaman Pappu</a>', 'bank', 4.1, 13),
(544, 'Uttara High School and College', 23.8709006, 90.3972596, '<a href=\"https://maps.google.com/maps/contrib/112453645377626185547/photos\">Abdullah al-mujahid</a>', 'school', 4.3, 13),
(545, 'Budget Hotel', 23.8699309, 90.3938555, '', 'lodging', 0, 13),
(546, 'jemtexltd', 23.8730792, 90.3940541, '', 'finance', 4, 13),
(547, 'Uttara Model School', 23.8716183, 90.3973971, '<a href=\"https://maps.google.com/maps/contrib/106671526848421578828/photos\">Uttara Model School</a>', 'school', 3.7, 13),
(548, 'ElanceIT', 23.871327, 90.39716, '<a href=\"https://maps.google.com/maps/contrib/109695941435495745341/photos\">ElanceIT</a>', 'point_of_interest', 4.7, 13),
(549, 'BRAC Bank Limited ATM', 23.8695522, 90.3935391, '<a href=\"https://maps.google.com/maps/contrib/102616886301050511904/photos\">Asive Chowdhury</a>', 'atm', 4.2, 13),
(550, 'Eastern Bank Limited ATM', 23.8684889, 90.3936168, '<a href=\"https://maps.google.com/maps/contrib/101125321244312960784/photos\">Hamidul Arefin Rizu</a>', 'atm', 4.1, 13),
(551, 'Milkyway Soft', 23.86916, 90.396766, '', 'point_of_interest', 0, 13),
(552, 'Tanjimul Ummah Alim Madrasah', 23.8721374, 90.3978308, '', 'school', 4.2, 13),
(553, 'International Education Centre', 23.870582, 90.395274, '<a href=\"https://maps.google.com/maps/contrib/100147735329150431142/photos\">International Education ', 'school', 4.8, 13),
(554, 'Marketa2z.com', 23.8682669, 90.3960872, '<a href=\"https://maps.google.com/maps/contrib/115136405390023246606/photos\">Marketa2z.com</a>', 'point_of_interest', 0, 13),
(555, 'Euro Tech Bangladesh', 23.8726181, 90.3949982, '', 'point_of_interest', 0, 13),
(556, 'NEW LOOK FABRICS / NEW LOOK International', 23.872694, 90.397169, '', 'point_of_interest', 0, 13),
(557, 'Dutch-Bangla Bank Limited ATM', 23.8647543, 90.39429, '<a href=\"https://maps.google.com/maps/contrib/102577011780154106588/photos\">Uzzal Hossen</a>', 'atm', 3.5, 14),
(558, 'Dutch-Bangla Bank Limited ATM', 23.8686041, 90.3936455, '<a href=\"https://maps.google.com/maps/contrib/106898798030706536087/photos\">Mohd. Abdul Awal</a>', 'atm', 3.4, 14),
(559, 'Platinum Home', 23.8695479, 90.3910178, '', 'point_of_interest', 3.8, 14),
(560, 'Baitul Aman Jame Masjid', 23.8684861, 90.3885656, '<a href=\"https://maps.google.com/maps/contrib/111833917403775119487/photos\">Masbahul Alam</a>', 'mosque', 4.6, 14),
(561, 'VERITAS', 23.86494, 90.390808, '', 'school', 3, 14),
(562, 'Southeast Bank Limited ATM', 23.866001, 90.388791, '<a href=\"https://maps.google.com/maps/contrib/103822217816415740415/photos\">JAHANGIR ALAM</a>', 'atm', 4.3, 14),
(563, 'Eastern Bank Limited ATM', 23.8684889, 90.3936168, '<a href=\"https://maps.google.com/maps/contrib/101125321244312960784/photos\">Hamidul Arefin Rizu</a>', 'atm', 4.1, 14),
(564, 'Soft Tech Innovation Ltd', 23.867285, 90.3905344, '<a href=\"https://maps.google.com/maps/contrib/113336386612459397243/photos\">SM SOFT TECH</a>', 'point_of_interest', 4.7, 14),
(565, 'Western Educare International', 23.8673458, 90.3916692, '', 'school', 0, 14),
(566, 'RsB', 23.866266, 90.391289, '', 'point_of_interest', 3, 14),
(567, 'Sunnyside School', 23.8663787, 90.3912679, '<a href=\"https://maps.google.com/maps/contrib/109268677075934691316/photos\">Akhtar Hossain</a>', 'school', 4.3, 14),
(568, 'Corporate HR Excellence Bangladesh', 23.8660391, 90.3917417, '', 'point_of_interest', 3.7, 14),
(569, 'Nayantti Apparel Ltd', 23.8659747, 90.3917943, '', 'point_of_interest', 5, 14),
(570, 'MAX SECURE LIMITED', 23.8671619, 90.3914312, '', 'point_of_interest', 4.5, 14),
(571, 'Premier Housing Developments Ltd.', 23.8661967, 90.3899744, '', 'general_contractor', 5, 14),
(572, 'pipra', 23.867287, 90.3905162, '<a href=\"https://maps.google.com/maps/contrib/104648351918500207513/photos\">pipra24</a>', 'clothing_store', 5, 14),
(573, 'Emporium Technology Ltd', 23.867287, 90.390516, '<a href=\"https://maps.google.com/maps/contrib/116422710167952458944/photos\">Emporium Technology Ltd<', 'point_of_interest', 4.2, 14),
(574, 'Peace School Dhaka', 23.8654034, 90.3909084, '<a href=\"https://maps.google.com/maps/contrib/108653184278869461995/photos\">Peace School</a>', 'school', 3.7, 14),
(575, 'FunPark Kidsâ€™ Learning Harbor', 23.8668737, 90.3920746, '<a href=\"https://maps.google.com/maps/contrib/100593185257052542162/photos\">FunPark PreSchool</a>', 'school', 4, 14),
(576, 'Hadid Corporation.', 23.8667372, 90.3922288, '', 'point_of_interest', 0, 14),
(577, 'Jagannath University', 23.7083125, 90.4113225, '<a href=\"https://maps.google.com/maps/contrib/112249457162506886033/photos\">Nazmul sagor</a>', 'university', 4.4, 15),
(578, 'muzicmad', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/109604309566295406503/photos\">khan ashik</a>', 'electronics_store', 5, 15),
(579, 'Today BD', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/106597495768526010960/photos\">Niraj Bhusal</a>', 'point_of_interest', 4.2, 15),
(580, 'Aimraj', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/104517801582234610930/photos\">sb sohag</a>', 'electronics_store', 4.5, 15),
(581, 'self', 23.709921, 90.407143, '', 'point_of_interest', 2, 15),
(582, 'Crucial solution', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(583, 'Tour In Bangladesh', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/114710318033605097591/photos\">kazi ponir</a>', 'travel_agency', 4, 15),
(584, 'Job Loader, à¦œà¦¬ à¦²à§‹à¦¡à¦¾à¦°', 23.710336, 90.406794, '', 'point_of_interest', 1, 15),
(585, 'bd Photoshop Tutorial', 23.709921, 90.407143, '', 'point_of_interest', 4, 15),
(586, 'Dhaka Forex', 23.709921, 90.407143, '', 'point_of_interest', 1, 15),
(587, 'Prime Bank Limited', 23.709921, 90.407143, '', 'bank', 4.2, 15),
(588, 'Caltrax Corporation', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(589, 'E Lab', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(590, 'tapan the king', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/104084809530144344534/photos\">sultan mahamud Tapan</a>', 'jewelry_store', 0, 15),
(591, 'Moneybook2u.Com | Social Network and Export-Import Business', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/117516048210588101817/photos\">Moneybook2u.Com | Social', 'point_of_interest', 0, 15),
(592, 'First Security Islami Bank Ltd.', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/103253318584723986290/photos\">mohammad shafaeat ullah ', 'bank', 0, 15),
(593, 'Nano Information Technology (Nanosoft)', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/105215128909175172884/photos\">Nano Information Technol', 'electronics_store', 0, 15),
(594, 'Titan Americas', 23.709921, 90.407143, '', 'point_of_interest', 0, 15),
(595, 'M.K.Electronics', 23.708548, 90.408865, '<a href=\"https://maps.google.com/maps/contrib/113166481180997459172/photos\">Hasan Ahmed</a>', 'electronics_store', 4.1, 15),
(596, 'PLUS SOLUTION', 23.709921, 90.407143, 'Photos are copyrighted by their owners', 'point_of_interest', 0, 15),
(597, 'Babylon Hotel & Serviced Apartments', 23.813714, 90.415222, '<a href=\"https://maps.google.com/maps/contrib/115717687060370220161/photos\">Babylon Hotel &amp; Serv', 'lodging', 3.9, 16),
(598, 'House', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/112832557214090154114/photos\">Tanzillur Rahman</a>', 'restaurant', 4, 16),
(599, 'MM Group', 23.812374, 90.412368, '<a href=\"https://maps.google.com/maps/contrib/100701293559743578797/photos\">MM Group</a>', 'point_of_interest', 0, 16),
(600, 'Glazed', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/101437021371206449368/photos\">Taufique Joarder</a>', 'bakery', 4.2, 16),
(601, 'AsiaInspection', 23.8102856, 90.4124813, '', 'point_of_interest', 5, 16),
(602, 'Monsoon Travel Management Company Ltd.', 23.8099321, 90.4130796, '', 'travel_agency', 0, 16),
(603, 'Rose Sweaters Ltd (HO)', 23.81045, 90.412989, '<a href=\"https://maps.google.com/maps/contrib/115747380291635600454/photos\">Rasel khan</a>', 'point_of_interest', 1, 16),
(604, 'Apparel manufacturing & Management Company', 23.810907, 90.413618, '', 'point_of_interest', 0, 16),
(605, 'IYOTEX BD LTD', 23.810936, 90.413693, '', 'point_of_interest', 0, 16),
(606, 'Gold Man Assets Ltd.', 23.8114923, 90.413144, '', 'point_of_interest', 4, 16),
(607, 'Embedded Engineering And Robotics Technology Ltd.', 23.8114741, 90.4128872, '<a href=\"https://maps.google.com/maps/contrib/104980350308870258294/photos\">Ankur Banik</a>', 'point_of_interest', 4.1, 16),
(608, 'Akter Properties Ltd.', 23.809974, 90.413339, '', 'point_of_interest', 0, 16),
(609, 'Purview Genesis Group', 23.8104955, 90.4122051, '', 'point_of_interest', 3, 16),
(610, 'SFI Business Center - Global', 23.810809, 90.413424, '<a href=\"https://maps.google.com/maps/contrib/108959140194715050848/photos\">zahirul asad</a>', 'point_of_interest', 0, 16),
(611, 'Grameen CyberNet Ltd.', 23.8114213, 90.4122528, '<a href=\"https://maps.google.com/maps/contrib/116890489596705901279/photos\">Nafees Imtiaz</a>', 'point_of_interest', 4, 16),
(612, 'Baridhara DOHS Mosque', 23.8132659, 90.4103459, '<a href=\"https://maps.google.com/maps/contrib/108899577731462005627/photos\">Riyad Hasan</a>', 'mosque', 4.6, 16),
(613, 'Web Team Office', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/110484888157267513901/photos\">Web Team Office</a>', 'point_of_interest', 5, 16),
(614, 'Phase 3 Solution', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/101126013848431099768/photos\">Phase 3 Solution</a>', 'point_of_interest', 5, 16),
(615, 'Design Ace Ltd.', 23.8126545, 90.4114337, '<a href=\"https://maps.google.com/maps/contrib/110770460039252827580/photos\">Tarik Mohammed</a>', 'point_of_interest', 5, 16),
(616, 'MoneyGram', 23.8670386, 90.366899, '<a href=\"https://maps.google.com/maps/contrib/110014197797015034800/photos\">MoneyGram</a>', 'finance', 3.5, 17),
(617, 'RedOrange Limited', 23.869099, 90.3695514, '<a href=\"https://maps.google.com/maps/contrib/112584583114053017321/photos\">RedOrange Limited</a>', 'point_of_interest', 5, 17),
(618, 'Clipping Path Masterly', 23.8686756, 90.3724985, '<a href=\"https://maps.google.com/maps/contrib/103786125004952713955/photos\">Abdullah Sayeed</a>', 'point_of_interest', 0, 17),
(619, 'Turkish Kabab Resturant', 23.8675408, 90.3681201, '<a href=\"https://maps.google.com/maps/contrib/103115779582122235018/photos\">Ashikur Rahman Ratul</a>', 'restaurant', 4, 17),
(620, 'Dia Bari - Bot tola', 23.8679262, 90.3673637, '<a href=\"https://maps.google.com/maps/contrib/113446630457649983983/photos\">MD. Imran Hossain</a>', 'park', 4.1, 17),
(621, 'Energy Water Garden', 23.8652122, 90.3672973, '<a href=\"https://maps.google.com/maps/contrib/115099409806043891781/photos\">Rakib Khan Orpo</a>', 'park', 4.1, 17),
(622, 'Dia Bari Nouka Ghat', 23.8661329, 90.3698917, '<a href=\"https://maps.google.com/maps/contrib/104594717662321906215/photos\">Sanji Gazi</a>', 'point_of_interest', 4.1, 17),
(623, 'Chaikhana Kebab & Grill', 23.8693314, 90.3673044, '<a href=\"https://maps.google.com/maps/contrib/108451208189540050832/photos\">Mahmudul Hasan Sakib</a>', 'restaurant', 2.3, 17),
(624, 'Chaikhana', 23.8693627, 90.3671931, '<a href=\"https://maps.google.com/maps/contrib/112477843943416718830/photos\">MAMUN</a>', 'restaurant', 3.3, 17),
(625, 'Diabari Health Center', 23.8697153, 90.3680976, '<a href=\"https://maps.google.com/maps/contrib/115168713720473269682/photos\">ovee I</a>', 'hospital', 5, 17),
(626, 'ORCA Complex', 23.8700844, 90.3674157, '<a href=\"https://maps.google.com/maps/contrib/116599325842466029121/photos\">Md. Mostaq Ahmed</a>', 'point_of_interest', 4.8, 17),
(627, 'SI South Palace', 23.8699078, 90.3659901, '<a href=\"https://maps.google.com/maps/contrib/106735656807102164356/photos\">Fayyazul Haque khan</a>', 'point_of_interest', 0, 17),
(628, 'à¦¬à¦¾à¦¤à¦¾à¦¸ à¦šà¦¾à¦šà¦¾à¦° à¦§à¦¾à¦¬à¦¾', 23.8706688, 90.3674636, '<a href=\"https://maps.google.com/maps/contrib/118222093288299230251/photos\">Mohammad Golam Mostafa</', 'restaurant', 4, 17),
(629, 'HEARING REHAB CENTER', 23.868676, 90.372499, '<a href=\"https://maps.google.com/maps/contrib/100738633566409169387/photos\">HEARING REHAB CENTER</a>', 'store', 0, 17),
(630, 'Uttara New Sector Bridge', 23.8647446, 90.3659821, '', 'premise', 0, 17),
(631, 'DESCO Site Diabari Sholahati', 23.8688365, 90.3648344, '<a href=\"https://maps.google.com/maps/contrib/111103040286914769162/photos\">ASIF AL AZAD</a>', 'point_of_interest', 0, 17),
(632, 'Jagannath University', 23.7083125, 90.4113225, '<a href=\"https://maps.google.com/maps/contrib/112249457162506886033/photos\">Nazmul sagor</a>', 'university', 4.4, 18),
(633, 'muzicmad', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/109604309566295406503/photos\">khan ashik</a>', 'electronics_store', 5, 18),
(634, 'Today BD', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/106597495768526010960/photos\">Niraj Bhusal</a>', 'point_of_interest', 4.2, 18),
(635, 'Aimraj', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/104517801582234610930/photos\">sb sohag</a>', 'electronics_store', 4.5, 18),
(636, 'self', 23.709921, 90.407143, '', 'point_of_interest', 2, 18),
(637, 'Crucial solution', 23.709921, 90.407143, '', 'point_of_interest', 0, 18),
(638, 'Tour In Bangladesh', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/114710318033605097591/photos\">kazi ponir</a>', 'travel_agency', 4, 18),
(639, 'Job Loader, à¦œà¦¬ à¦²à§‹à¦¡à¦¾à¦°', 23.710336, 90.406794, '', 'point_of_interest', 1, 18),
(640, 'bd Photoshop Tutorial', 23.709921, 90.407143, '', 'point_of_interest', 4, 18),
(641, 'Dhaka Forex', 23.709921, 90.407143, '', 'point_of_interest', 1, 18),
(642, 'Prime Bank Limited', 23.709921, 90.407143, '', 'bank', 4.2, 18),
(643, 'Caltrax Corporation', 23.709921, 90.407143, '', 'point_of_interest', 0, 18),
(644, 'E Lab', 23.709921, 90.407143, '', 'point_of_interest', 0, 18),
(645, 'tapan the king', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/104084809530144344534/photos\">sultan mahamud Tapan</a>', 'jewelry_store', 0, 18),
(646, 'Moneybook2u.Com | Social Network and Export-Import Business', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/117516048210588101817/photos\">Moneybook2u.Com | Social', 'point_of_interest', 0, 18),
(647, 'First Security Islami Bank Ltd.', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/103253318584723986290/photos\">mohammad shafaeat ullah ', 'bank', 0, 18),
(648, 'Nano Information Technology (Nanosoft)', 23.709921, 90.407143, '<a href=\"https://maps.google.com/maps/contrib/105215128909175172884/photos\">Nano Information Technol', 'electronics_store', 0, 18),
(649, 'Titan Americas', 23.709921, 90.407143, '', 'point_of_interest', 0, 18),
(650, 'M.K.Electronics', 23.708548, 90.408865, '<a href=\"https://maps.google.com/maps/contrib/113166481180997459172/photos\">Hasan Ahmed</a>', 'electronics_store', 4.1, 18),
(651, 'PLUS SOLUTION', 23.709921, 90.407143, 'Photos are copyrighted by their owners', 'point_of_interest', 0, 18),
(652, 'Hotel de Castle', 23.794795, 90.4035692, '<a href=\"https://maps.google.com/maps/contrib/112843574698058837965/photos\">Tuhidul islam Mridul</a>', 'lodging', 3.8, 19),
(653, 'Marino Hotel Banani', 23.79933, 90.4073272, '<a href=\"https://maps.google.com/maps/contrib/101877895908668786792/photos\">Ð¡ÐµÑ€Ð³ÐµÐ¹ Ð“Ð»Ð°Ð·ÐºÐ', 'lodging', 4, 19),
(654, 'Hotel Sweet Dream', 23.7934126, 90.4074624, '<a href=\"https://maps.google.com/maps/contrib/103877232006001265440/photos\">Billal Hossain</a>', 'lodging', 4.1, 19),
(655, 'Quality Inn', 23.7967872, 90.4106368, '<a href=\"https://maps.google.com/maps/contrib/102109233717354650317/photos\">Biplob Biplob</a>', 'lodging', 3.7, 19),
(656, 'Royal Park', 23.7970459, 90.4048312, '<a href=\"https://maps.google.com/maps/contrib/100072423149710993659/photos\">Dean Lim</a>', 'lodging', 4.1, 19),
(657, 'Southeast University BBA Campus', 23.7945972, 90.4047276, '<a href=\"https://maps.google.com/maps/contrib/110874094855255188159/photos\">Shahzad ali Khurram</a>', 'university', 4.1, 19),
(658, 'Boomers Cafe Banani Branch', 23.7937869, 90.4044515, '<a href=\"https://maps.google.com/maps/contrib/107387199914092163672/photos\">Syed Ziaul Habib</a>', 'restaurant', 4, 19),
(659, 'Green House Guest House', 23.801117, 90.4079667, '<a href=\"https://maps.google.com/maps/contrib/114961232641196714520/photos\">Hafiz Shuputra</a>', 'lodging', 3.3, 19),
(660, 'Hotel Lake View Plaza', 23.796821, 90.4050484, '<a href=\"https://maps.google.com/maps/contrib/108899577731462005627/photos\">Riyad Hasan</a>', 'lodging', 3.6, 19),
(661, 'The Bengal Tours Ltd.', 23.7986628, 90.4039889, '', 'point_of_interest', 4.5, 19),
(662, 'Mid Asia Cars', 23.7949607, 90.4045737, '', 'car_dealer', 4.5, 19);
INSERT INTO `poi` (`id`, `name`, `lattitude`, `longitude`, `photo`, `types`, `rating`, `rs_id`) VALUES
(663, 'Melange Coffee N Conversation', 23.795895, 90.408253, '<a href=\"https://maps.google.com/maps/contrib/101020675838377046359/photos\">MushfiQ Shishir</a>', 'restaurant', 4.1, 19),
(664, 'Desme Bangladesh', 23.7969745, 90.4071114, '<a href=\"https://maps.google.com/maps/contrib/115841290690219475496/photos\">Shahriar Iqbal Chowdhury', 'point_of_interest', 5, 19),
(665, 'Quality Institute of America', 23.7997635, 90.4070064, '', 'point_of_interest', 3, 19),
(666, 'Seoul Guest House', 23.7972431, 90.4049492, '<a href=\"https://maps.google.com/maps/contrib/113635959783612362710/photos\">Beautiful world</a>', 'lodging', 3, 19),
(667, 'Motor World Imports', 23.7982395, 90.4058558, '', 'car_dealer', 3.2, 19),
(668, 'SAOnline Bangladesh', 23.7960342, 90.4079262, '', 'point_of_interest', 5, 19),
(669, 'Silverstitches', 23.797196, 90.405708, '<a href=\"https://maps.google.com/maps/contrib/100563813685470644566/photos\">Purandi Lawannya</a>', 'point_of_interest', 0, 19),
(670, 'DevConsultants Limited (DevCon)', 23.797962, 90.405442, '<a href=\"https://maps.google.com/maps/contrib/114577533560217880203/photos\">DevConsultants Limited (', 'point_of_interest', 4.6, 19),
(671, 'Babylon Hotel & Serviced Apartments', 23.813714, 90.415222, '<a href=\"https://maps.google.com/maps/contrib/115717687060370220161/photos\">Babylon Hotel &amp; Serv', 'lodging', 3.9, 20),
(672, 'House', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/112832557214090154114/photos\">Tanzillur Rahman</a>', 'restaurant', 4, 20),
(673, 'MM Group', 23.812374, 90.412368, '<a href=\"https://maps.google.com/maps/contrib/100701293559743578797/photos\">MM Group</a>', 'point_of_interest', 0, 20),
(674, 'Glazed', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/101437021371206449368/photos\">Taufique Joarder</a>', 'bakery', 4.2, 20),
(675, 'AsiaInspection', 23.8102856, 90.4124813, '', 'point_of_interest', 5, 20),
(676, 'Monsoon Travel Management Company Ltd.', 23.8099321, 90.4130796, '', 'travel_agency', 0, 20),
(677, 'Rose Sweaters Ltd (HO)', 23.81045, 90.412989, '<a href=\"https://maps.google.com/maps/contrib/115747380291635600454/photos\">Rasel khan</a>', 'point_of_interest', 1, 20),
(678, 'Apparel manufacturing & Management Company', 23.810907, 90.413618, '', 'point_of_interest', 0, 20),
(679, 'IYOTEX BD LTD', 23.810936, 90.413693, '', 'point_of_interest', 0, 20),
(680, 'Gold Man Assets Ltd.', 23.8114923, 90.413144, '', 'point_of_interest', 4, 20),
(681, 'Embedded Engineering And Robotics Technology Ltd.', 23.8114741, 90.4128872, '<a href=\"https://maps.google.com/maps/contrib/104980350308870258294/photos\">Ankur Banik</a>', 'point_of_interest', 4.1, 20),
(682, 'Akter Properties Ltd.', 23.809974, 90.413339, '', 'point_of_interest', 0, 20),
(683, 'Purview Genesis Group', 23.8104955, 90.4122051, '', 'point_of_interest', 3, 20),
(684, 'SFI Business Center - Global', 23.810809, 90.413424, '<a href=\"https://maps.google.com/maps/contrib/108959140194715050848/photos\">zahirul asad</a>', 'point_of_interest', 0, 20),
(685, 'Grameen CyberNet Ltd.', 23.8114213, 90.4122528, '<a href=\"https://maps.google.com/maps/contrib/116890489596705901279/photos\">Nafees Imtiaz</a>', 'point_of_interest', 4, 20),
(686, 'Baridhara DOHS Mosque', 23.8132659, 90.4103459, '<a href=\"https://maps.google.com/maps/contrib/108899577731462005627/photos\">Riyad Hasan</a>', 'mosque', 4.6, 20),
(687, 'Web Team Office', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/110484888157267513901/photos\">Web Team Office</a>', 'point_of_interest', 5, 20),
(688, 'Phase 3 Solution', 23.810332, 90.412518, '<a href=\"https://maps.google.com/maps/contrib/101126013848431099768/photos\">Phase 3 Solution</a>', 'point_of_interest', 5, 20),
(689, 'Design Ace Ltd.', 23.8126545, 90.4114337, '<a href=\"https://maps.google.com/maps/contrib/110770460039252827580/photos\">Tarik Mohammed</a>', 'point_of_interest', 5, 20),
(690, 'Viquarunnisa Noon School and College Bashundhara Branch', 23.8205979, 90.4317413, '<a href=\"https://maps.google.com/maps/contrib/110660822343565480525/photos\">Md. Muzahid Bin-Mizan</a', 'school', 4.3, 21),
(691, 'HSBC Bank Limited ATM', 23.8220106, 90.4293195, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'atm', 4.8, 21),
(692, 'Anamika Karotoa', 23.8248725, 90.4312027, '<a href=\"https://maps.google.com/maps/contrib/101391725145184873359/photos\">Mahfuz Siddiqui</a>', 'point_of_interest', 3.5, 21),
(693, 'SnapShot', 23.822334, 90.433389, '<a href=\"https://maps.google.com/maps/contrib/110705391895031412986/photos\">SnapShot</a>', 'point_of_interest', 5, 21),
(694, 'Orange Visuals', 23.8228947, 90.4328986, '<a href=\"https://maps.google.com/maps/contrib/117026954003723073692/photos\">Orange Visuals</a>', 'point_of_interest', 0, 21),
(695, 'HSBC ATM Bashundhara', 23.8220168, 90.4293254, '<a href=\"https://maps.google.com/maps/contrib/110290886265328164855/photos\">Syed Naved Abdullah</a>', 'atm', 4.3, 21),
(696, 'Eastern Bank Limited ATM', 23.821596, 90.4293828, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'atm', 4.8, 21),
(697, 'SLOAN SQUARE', 23.8201713, 90.4318145, '<a href=\"https://maps.google.com/maps/contrib/105655298857197313292/photos\">Ahnaf Brinto</a>', 'point_of_interest', 5, 21),
(698, 'Physio Clinic', 23.8227619, 90.4344038, '', 'hospital', 5, 21),
(699, 'GeekHouse', 23.8250655, 90.4305589, '', 'lodging', 4.7, 21),
(700, 'Star Holidays Bashundhara', 23.8249772, 90.4346788, '<a href=\"https://maps.google.com/maps/contrib/107018379904451727718/photos\">Tayyebur Rahman Junaid</', 'travel_agency', 4.7, 21),
(701, 'American International University-Bangladesh', 23.8220711, 90.4274569, '<a href=\"https://maps.google.com/maps/contrib/104381660664275810553/photos\">Imam Hasan Rafsun</a>', 'university', 4.7, 21),
(702, 'Greenland Kaniz Evelina', 23.8193207, 90.4294045, '<a href=\"https://maps.google.com/maps/contrib/108448402556243933986/photos\">Mmjsumit</a>', 'point_of_interest', 0, 21),
(703, 'Greenland Raihan Castle', 23.81859, 90.4318483, '<a href=\"https://maps.google.com/maps/contrib/113373166723905912626/photos\">Amirul Islam</a>', 'point_of_interest', 0, 21),
(704, 'Bashundhara Convention Center 2', 23.8218308, 90.4290214, '<a href=\"https://maps.google.com/maps/contrib/115675456271849866148/photos\">Ramil Hossain</a>', 'point_of_interest', 3.6, 21),
(705, 'Chandralok Abas Ltd.', 23.8226364, 90.4315996, '<a href=\"https://maps.google.com/maps/contrib/115503337750676834684/photos\">Rakib Hasan</a>', 'general_contractor', 4, 21),
(706, 'Dot Interior', 23.8224066, 90.4329206, '<a href=\"https://maps.google.com/maps/contrib/102984367070577080432/photos\">Rhman Mahbub</a>', 'point_of_interest', 3.5, 21),
(707, 'Reflection', 23.8237258, 90.4323265, '', 'point_of_interest', 0, 21),
(708, 'Moonlight', 23.8214429, 90.432416, '', 'point_of_interest', 3, 21),
(709, 'RIGS INN', 23.7818807, 90.4141519, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'lodging', 3.8, 22),
(710, 'Purnima Restaurant', 23.7807648, 90.4168984, '<a href=\"https://maps.google.com/maps/contrib/113330631340155578455/photos\">Tasfin Chowdhury</a>', 'restaurant', 3.6, 22),
(711, 'Agni Systems Limited', 23.7805068, 90.4163324, '<a href=\"https://maps.google.com/maps/contrib/107437657389734272820/photos\">Prince Rahman</a>', 'point_of_interest', 4.5, 22),
(712, 'Converge Limited', 23.781182, 90.416085, '<a href=\"https://maps.google.com/maps/contrib/107749024136607360266/photos\">Converge Limited</a>', 'point_of_interest', 0, 22),
(713, 'Malaysia Airlines', 23.7827081, 90.4171676, '<a href=\"https://maps.google.com/maps/contrib/118292777726534178700/photos\">Syed Shafqat Rabbi</a>', 'point_of_interest', 3.4, 22),
(714, 'Spark Gear Limited', 23.7819745, 90.4172799, '<a href=\"https://maps.google.com/maps/contrib/116508861149927004230/photos\">Allen Joseph Gomes</a>', 'clothing_store', 4.1, 22),
(715, 'Hotel De Crystal Crown', 23.7816293, 90.4140481, '<a href=\"https://maps.google.com/maps/contrib/115354697955564735574/photos\">Michael Bouy</a>', 'lodging', 3.6, 22),
(716, 'Hotel Center Point', 23.7766297, 90.413954, '', 'bar', 3.7, 22),
(717, 'Shahjalal Islami Bank Limited', 23.7802611, 90.4182569, '<a href=\"https://maps.google.com/maps/contrib/110521534845417388410/photos\">Samsuzzaman Riton</a>', 'bank', 3.8, 22),
(718, 'BRAC Bank Limited', 23.7751351, 90.4160462, '<a href=\"https://maps.google.com/maps/contrib/115129573728814462718/photos\">Md. Abu Hena Sazzad Bin ', 'bank', 3.8, 22),
(719, 'Export Import Bank Of Bangladesh Limited', 23.775654, 90.416557, '<a href=\"https://maps.google.com/maps/contrib/113331284469797275245/photos\">à¦à¦®. à¦®à¦¿à¦œà¦¾à¦¨à', 'bank', 4.4, 22),
(720, 'Siemens Bangladesh Limted', 23.7771158, 90.4162466, '<a href=\"https://maps.google.com/maps/contrib/108538477907272532712/photos\">diparun bhattacharyya</a', 'point_of_interest', 4.4, 22),
(721, 'Persona', 23.7801883, 90.418101, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'beauty_salon', 4.1, 22),
(722, 'Reckitt Benckiser Bangladesh Ltd', 23.7783224, 90.4171893, '<a href=\"https://maps.google.com/maps/contrib/104170844186005363017/photos\">Ariful Islam</a>', 'point_of_interest', 4.4, 22),
(723, 'Cadet College Club Limited', 23.778884, 90.415184, '<a href=\"https://maps.google.com/maps/contrib/111609921050107079630/photos\">Golam Kibria</a>', 'point_of_interest', 4.4, 22),
(724, 'One97 Communication Bangladesh', 23.7809612, 90.4160401, '', 'point_of_interest', 0, 22),
(725, 'Qatar Airways', 23.7774905, 90.4163861, '<a href=\"https://maps.google.com/maps/contrib/111208044373004775719/photos\">emran seu</a>', 'point_of_interest', 3.9, 22),
(726, 'International Finance Corporation', 23.7769095, 90.4160858, '', 'finance', 3, 22),
(727, 'Glaxo SmithKline Bangladesh Ltd', 23.7782854, 90.4169436, '<a href=\"https://maps.google.com/maps/contrib/106680213228138474871/photos\">Junayed Emran</a>', 'health', 4.2, 22),
(728, 'British High Commission', 23.7989724, 90.4194403, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'embassy', 4.5, 23),
(729, 'Baridhara Lakeside Rajuk Park', 23.7967981, 90.4192396, '<a href=\"https://maps.google.com/maps/contrib/103785358507313746836/photos\">Tareq Salahuddin</a>', 'park', 4.3, 23),
(730, 'Asia Pacific Hotel', 23.8013317, 90.4214749, '<a href=\"https://maps.google.com/maps/contrib/107403001536741962162/photos\">Dhriti Sundar Karmakar</', 'lodging', 3.8, 23),
(731, 'Nascent Gardenia Hotel', 23.8042668, 90.4210693, '<a href=\"https://maps.google.com/maps/contrib/116508861149927004230/photos\">Allen Joseph Gomes</a>', 'lodging', 3.9, 23),
(732, 'Katalyst', 23.8035008, 90.4195411, '', 'point_of_interest', 4.4, 23),
(733, 'American International School Dhaka', 23.8007999, 90.4194362, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'school', 4.2, 23),
(734, 'Embassy of Japan in Bangladesh', 23.79897, 90.4203241, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'embassy', 4.6, 23),
(735, 'High Commission of Canada', 23.796459, 90.4206923, '<a href=\"https://maps.google.com/maps/contrib/107260561261604967583/photos\">Dark Circle</a>', 'embassy', 4.3, 23),
(736, 'American International School Dhaka', 23.8008316, 90.4194403, '<a href=\"https://maps.google.com/maps/contrib/115588445286171732870/photos\">Snal Family</a>', 'school', 4.1, 23),
(737, 'InfoLink à¦‡à¦¨à¦«à§‹ à¦²à¦¿à¦‚à¦•', 23.802945, 90.422816, '<a href=\"https://maps.google.com/maps/contrib/101281767849258144660/photos\">InfoLink à¦‡à¦¨à¦«à§‹ à¦', 'electronics_store', 4.7, 23),
(738, 'DESCO', 23.79766, 90.4246171, '<a href=\"https://maps.google.com/maps/contrib/115029425601246152704/photos\">Zahid Rasel</a>', 'point_of_interest', 3.8, 23),
(739, 'The Korea International Cooperation Agency(KOICA)', 23.7978125, 90.4215467, '<a href=\"https://maps.google.com/maps/contrib/115738842673732634958/photos\">kamal ibn saif</a>', 'point_of_interest', 5, 23),
(740, 'Elite Security Services Limited', 23.800963, 90.423413, '<a href=\"https://maps.google.com/maps/contrib/106924267507720582497/photos\">Ruhul Amin</a>', 'point_of_interest', 4.5, 23),
(741, 'HRKites', 23.802867, 90.420047, '', 'point_of_interest', 4.3, 23),
(742, 'Appnometry Ltd.', 23.8023084, 90.4209137, '<a href=\"https://maps.google.com/maps/contrib/100715051611063455853/photos\">Sorot Khan</a>', 'point_of_interest', 4, 23),
(743, 'explore corporation', 23.7994077, 90.4184568, '', 'park', 3.5, 23),
(744, 'The PriSe Power & Energy Limited', 23.7997872, 90.418497, '', 'point_of_interest', 0, 23),
(745, 'Atrium', 23.80249, 90.4229254, '<a href=\"https://maps.google.com/maps/contrib/105271665042880946383/photos\">Masud Chowdhury</a>', 'restaurant', 4, 23),
(746, 'Qubee Wimax', 23.7998984, 90.420766, '<a href=\"https://maps.google.com/maps/contrib/101111941132360918538/photos\">Appon Islam</a>', 'point_of_interest', 0, 23),
(747, 'Green University of Bangladesh', 23.7869613, 90.3775339, '<a href=\"https://maps.google.com/maps/contrib/100934171519555948898/photos\">Ashraful Alam</a>', 'university', 4, 24),
(748, 'Samsung Electra International', 23.7876442, 90.3766717, '<a href=\"https://maps.google.com/maps/contrib/108213844028752025043/photos\">MD RAZIB KHAN</a>', 'electronics_store', 4.1, 24),
(749, 'National Bank Limited, Rokeya Sarani Branch', 23.7866311, 90.3772253, '<a href=\"https://maps.google.com/maps/contrib/100312494095263163184/photos\">Arafatul Islam</a>', 'bank', 4.5, 24),
(750, 'Next Sourcing Limited', 23.7890251, 90.3765728, '', 'point_of_interest', 4, 24),
(751, 'Net Cyber Technology', 23.7866456, 90.3776127, '', 'point_of_interest', 0, 24),
(752, 'AzizulSoft', 23.7866462, 90.3776126, '', 'point_of_interest', 0, 24),
(753, 'Green Blood Club', 23.7869717, 90.377494, '', 'point_of_interest', 0, 24),
(754, 'Great Writing Instruments Company', 23.7866373, 90.3776305, '<a href=\"https://maps.google.com/maps/contrib/101515676482909807016/photos\">Great Writing Instrument', 'store', 5, 24),
(755, 'Acer Customer Care Center', 23.7866536, 90.3775745, '<a href=\"https://maps.google.com/maps/contrib/104925572934122875639/photos\">bappi nfs</a>', 'point_of_interest', 3.7, 24),
(756, 'North Western College', 23.7867299, 90.3775006, '<a href=\"https://maps.google.com/maps/contrib/100312494095263163184/photos\">Arafatul Islam</a>', 'point_of_interest', 4.3, 24),
(757, 'Bengal Agencies', 23.7867794, 90.3771964, '', 'home_goods_store', 0, 24),
(758, 'Lead IT', 23.7864653, 90.3772594, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'point_of_interest', 0, 24),
(759, 'UCC', 23.7867879, 90.3770914, '<a href=\"https://maps.google.com/maps/contrib/101828189509382068696/photos\">Golam Roquib</a>', 'point_of_interest', 3.5, 24),
(760, 'CGA Bangladesh', 23.7856044, 90.3779833, '<a href=\"https://maps.google.com/maps/contrib/115414908234811252069/photos\">najmul alam</a>', 'point_of_interest', 0, 24),
(761, 'Gigabyte Technology Company Limited', 23.7863642, 90.3772722, '<a href=\"https://maps.google.com/maps/contrib/116249220594432973707/photos\">J A</a>', 'electronics_store', 4.5, 24),
(762, 'Digital Track', 23.786255, 90.377327, '<a href=\"https://maps.google.com/maps/contrib/105798228317068850602/photos\">Digital Track</a>', 'point_of_interest', 5, 24),
(763, 'Jahir Smart Tower', 23.7859875, 90.3774981, '<a href=\"https://maps.google.com/maps/contrib/100312494095263163184/photos\">Arafatul Islam</a>', 'point_of_interest', 5, 24),
(764, 'MoneyGram', 23.7899467, 90.3780439, '<a href=\"https://maps.google.com/maps/contrib/102219252029801745588/photos\">MoneyGram</a>', 'finance', 3.8, 24),
(765, 'Youth Group', 23.78867, 90.376983, '<a href=\"https://maps.google.com/maps/contrib/112656993949997213327/photos\">Manas Mistry</a>', 'point_of_interest', 4.1, 24),
(766, 'Tropical Daisy', 23.7893979, 90.4133194, '<a href=\"https://maps.google.com/maps/contrib/110242101723524532338/photos\">Tropical Daisy</a>', 'lodging', 3.3, 25),
(767, 'Long Beach Suites Dhaka', 23.789926, 90.420301, '<a href=\"https://maps.google.com/maps/contrib/118376530638424320059/photos\">Soyeb Hassan</a>', 'lodging', 4.2, 25),
(768, 'Trust Bank Limited', 23.78862, 90.4166588, '<a href=\"https://maps.google.com/maps/contrib/107773382207762368665/photos\">Md.Arifur Rahman</a>', 'bank', 4, 25),
(769, 'Hotel Golden Deer (Lake View)', 23.789588, 90.413351, '<a href=\"https://maps.google.com/maps/contrib/112928935378486839447/photos\">Hotel Golden Deer (Lake ', 'lodging', 3.4, 25),
(770, 'Premier Bank Limited', 23.79308, 90.4155004, '<a href=\"https://maps.google.com/maps/contrib/100184291643871645549/photos\">Zeenat Islam</a>', 'bank', 4.1, 25),
(771, 'SATV', 23.7871948, 90.4179874, '<a href=\"https://maps.google.com/maps/contrib/112154922664457020260/photos\">SATV</a>', 'point_of_interest', 4.2, 25),
(772, 'Gadget & Gear', 23.7885388, 90.4163771, '<a href=\"https://maps.google.com/maps/contrib/115129573728814462718/photos\">Md. Abu Hena Sazzad Bin ', 'electronics_store', 3.7, 25),
(773, 'Shahabuddin Medical College Hospital', 23.7895128, 90.4197165, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'hospital', 3.8, 25),
(774, 'Century Park Hotel in Gulshan Dhaka', 23.7856724, 90.4186784, '<a href=\"https://maps.google.com/maps/contrib/112934993831286849927/photos\">Century Park Residence</', 'lodging', 3.8, 25),
(775, 'Manarat Dhaka International College', 23.790416, 90.4209133, '<a href=\"https://maps.google.com/maps/contrib/116647054874586501310/photos\">Shaifullah Mirza</a>', 'university', 4.1, 25),
(776, 'Activating Village Courts in Bangladesh', 23.7903324, 90.4174519, '<a href=\"https://maps.google.com/maps/contrib/107567645443471736029/photos\">Sajid Huq</a>', 'point_of_interest', 4.2, 25),
(777, 'CUAC', 23.7879285, 90.416846, '', 'point_of_interest', 4.7, 25),
(778, 'Embassy of Brazil', 23.7878923, 90.4163466, '<a href=\"https://maps.google.com/maps/contrib/100878278244554395614/photos\">Ziad Hossain</a>', 'embassy', 3.8, 25),
(779, 'Cats Eye', 23.7895782, 90.4160255, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'clothing_store', 4.1, 25),
(780, 'Ericsson', 23.7882422, 90.4168996, '<a href=\"https://maps.google.com/maps/contrib/103932795989180463054/photos\">Ornob Ripon</a>', 'point_of_interest', 4.3, 25),
(781, 'IZUMI Japanese Kitchen', 23.7885051, 90.4206099, '<a href=\"https://maps.google.com/maps/contrib/101664547497950129605/photos\">IZUMI Japanese Kitchen</', 'restaurant', 4.4, 25),
(782, 'Samsung Smartphone Cafe', 23.7889012, 90.4165844, '<a href=\"https://maps.google.com/maps/contrib/102928139062279172353/photos\">Mahmudur Rahman</a>', 'store', 4.2, 25),
(783, 'Consulate of the Republic of Singapore', 23.7878937, 90.4163607, '', 'embassy', 4.3, 25),
(784, 'bti - building technology & ideas ltd', 23.7892808, 90.4194987, '<a href=\"https://maps.google.com/maps/contrib/107099286706862550232/photos\">bti - building technolog', 'real_estate_agency', 4.3, 25),
(785, 'Istanbul Turkish Restaurant', 23.788744, 90.4194173, '<a href=\"https://maps.google.com/maps/contrib/114838252423870245054/photos\">Ishtiak Hossain Jami</a>', 'restaurant', 3.9, 25);

-- --------------------------------------------------------

--
-- Table structure for table `real_estate`
--

CREATE TABLE `real_estate` (
  `rs_id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(100) NOT NULL,
  `lattitude` float NOT NULL,
  `longitude` float NOT NULL,
  `p_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `d_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_estate`
--

INSERT INTO `real_estate` (`rs_id`, `name`, `price`, `lattitude`, `longitude`, `p_id`, `image`, `d_id`) VALUES
(1, '2048 sq-ft.(Type-A), 3 Bedroom Apartment for Sale in Navana City Within Urban Dale (Block-1)', 20480000, 23.805, 90.3835, 1, 'd1.jpg', 1),
(2, 'Apartment for Sale in Acme Casa Hermanas, Extended Pallabi', 11500000, 23.8244, 90.3617, 1, 'd2.jpg', 1),
(3, 'Apartment for Sale in Navana Probani Ridgedale (Block-1)', 22460000, 23.8184, 90.3742, 1, 'd3.jpg', 1),
(4, 'Apartment for Sale in IMAGINE EASTWOOD', 18500000, 23.8138, 90.4277, 4, 'd4.jpg', 1),
(5, 'Apartment for Sale in Bijoy Rakeen City', 18720000, 23.8045, 90.3816, 1, 'd5.jpg', 1),
(6, 'Apartment for Sale in Uttara', 21850000, 23.863, 90.4018, 3, 'd6.jpg', 1),
(7, 'Apartment for Sale in Landmark JKH Complex', 65000000, 23.7965, 90.4128, 5, 'd14.jpg', 1),
(8, 'Apartment for Sale in Bijoy Rakeen City', 15530000, 23.8045, 90.3816, 1, 'blue.jpg', 1),
(9, 'apartment for sale in Sufia South Point', 30000000, 23.8216, 90.4311, 5, 'd2.jpg', 1),
(10, 'Apartment for sale in Priyoprangon Bashantika', 0, 23.757, 90.4643, 6, 'sd.jpg', 1),
(11, 'Apartment for Sale in Navana Probani Ridgedale (Block-5)', 141100000, 23.8184, 90.3742, 1, 'din.jpg', 1),
(12, '1665 sq-ft.(Type-A), 3 Bedroom Apartment for Sale in Civil Rahmat', 19147500, 23.8669, 90.4045, 3, 'd7.jpg', 1),
(13, '1675 sq-ft., 3 Bedroom Apartment for sale in ABC Neel Kunja', 19262500, 23.8709, 90.3953, 3, 'd8.jpg', 1),
(14, '1128 sq-ft. (Type-D), 3 Bedroom Apartment for Sale in PPL HOSNE ARA VILLA, Uttara', 12972000, 23.8664, 90.391, 3, 'd9.jpg', 1),
(15, '1381 sq-ft. (Type-C), 3 Bedroom Apartment for Sale in Sagufta De Happy', 15881500, 23.7099, 90.4071, 3, 'd10.jpg', 1),
(16, '1500 sq-ft. (Type-E), 3 Bedroom Apartment for Sale in Joynal Tower', 17250000, 23.8103, 90.4125, 3, 'd11.jpg', 1),
(17, '1640 sq-ft. (Type-C), 3 Bedroom Apartment for Sale in Toma Sunflower', 18860000, 23.8668, 90.3681, 3, 'd12.jpg', 1),
(18, '1811 sq-ft. 3 bedroom full furnished Apartment for Sale ', 40000000, 23.7099, 90.4071, 5, 'd19.jpg', 1),
(19, '2090 sq-ft. 3 Bedroom Ready Apartment for Sale', 47000000, 23.7976, 90.4069, 5, 'building.jpg', 1),
(20, '1054 sq-ft. (Type-A), 3 Bedroom Apartment for Sale in MAK Bhaban', 24000000, 23.8103, 90.4125, 5, 'blue.jpg', 1),
(21, '2644 sq-ft. 4 Bedroom Luxurious Apartment for Sale ', 60000000, 23.8226, 90.4316, 5, 'd13.jpg', 1),
(22, '2400 sq-ft. 3 bedroom Apartment for sale in Bay\'s Brookwood', 43200000, 23.7786, 90.4172, 5, 'd17.jpg', 1),
(23, '1100 sq-ft. (Type-A), 3 Bedroom Apartment for Sale in Model Dewan Monjil', 24500000, 23.7999, 90.4208, 5, 'd18.jpg', 1),
(24, '2400 sq-ft. 3 Bedroom Luxury Apartment for Sale', 50000000, 23.7877, 90.3799, 5, 'd16.jpg', 1),
(25, '1945 sq-ft. 3 Bedroom Apartment for sale', 45000000, 23.7896, 90.4179, 5, 'd20.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(100) NOT NULL,
  `rs_id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `hospitals` double NOT NULL,
  `environmental_surroundings` double NOT NULL,
  `roads` double NOT NULL,
  `electricity` double NOT NULL,
  `gas` double NOT NULL,
  `departmental_store` double NOT NULL,
  `school_colleges` double NOT NULL,
  `super_market` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `rs_id`, `user_id`, `hospitals`, `environmental_surroundings`, `roads`, `electricity`, `gas`, `departmental_store`, `school_colleges`, `super_market`) VALUES
(1, 3, 3, 4, 5, 3, 2, 4, 3, 4, 5),
(2, 2, 2, 5, 1, 3, 4, 5, 3, 2, 1),
(3, 9, 5, 3, 3, 5, 1, 4, 2, 4, 4),
(4, 6, 4, 2, 4, 2, 3, 3, 5, 3, 5),
(5, 8, 3, 3, 5, 5, 4, 4, 2, 3, 2),
(6, 11, 4, 4, 2, 3, 3, 3, 4, 5, 5),
(7, 5, 3, 4, 4, 4, 5, 3, 3, 2, 5),
(8, 10, 2, 3, 3, 2, 0, 4, 2, 1, 3),
(9, 4, 2, 3, 5, 1, 2, 4, 3, 5, 2),
(10, 7, 5, 3, 3, 4, 3, 2, 6, 4, 2),
(11, 10, 2, 3, 4, 2, 5, 5, 5, 4, 3),
(12, 11, 2, 3, 5, 3, 3, 4, 3, 0, 0),
(13, 8, 2, 5, 5, 4, 4, 4, 3, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rs_details`
--

CREATE TABLE `rs_details` (
  `id` int(100) NOT NULL,
  `rs_id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(100) NOT NULL,
  `size_sqft` int(100) NOT NULL,
  `room` int(100) NOT NULL,
  `bedroom` int(50) NOT NULL,
  `bath` int(50) NOT NULL,
  `floor` int(50) NOT NULL,
  `lift` varchar(50) NOT NULL,
  `generator` varchar(50) NOT NULL,
  `fire_exit` varchar(50) NOT NULL,
  `gas_connection` varchar(50) NOT NULL,
  `wasa_connection` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `map` mediumtext NOT NULL,
  `nearby_hos` mediumtext NOT NULL,
  `nearby_col` mediumtext NOT NULL,
  `nearby_shop` mediumtext NOT NULL,
  `d_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rs_details`
--

INSERT INTO `rs_details` (`id`, `rs_id`, `name`, `price`, `size_sqft`, `room`, `bedroom`, `bath`, `floor`, `lift`, `generator`, `fire_exit`, `gas_connection`, `wasa_connection`, `image`, `map`, `nearby_hos`, `nearby_col`, `nearby_shop`, `d_id`) VALUES
(1, 1, 'Apartment for Sale in Navana City Within Urban Dale (Block-1)', 0, 2048, 6, 3, 3, 3, 'Yes', 'yes', 'yes', 'yes', 'yes', 'd1.jpg', '!1m18!1m12!1m3!1d3650.3821884233575!2d90.38131131442837!3d23.805004892606153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ4JzE4LjAiTiA5MMKwMjMnMDAuNiJF!5e0!3m2!1sen!2sbd!4v1510838909612', '!1m16!1m12!1m3!1d29203.047800629924!2d90.36599034631874!3d23.8050480619071!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shospitals!5e0!3m2!1sen!2sbd!4v1510838980004', '!1m16!1m12!1m3!1d29203.047800629924!2d90.36599034631874!3d23.8050480619071!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sschools!5e0!3m2!1sen!2sbd!4v1510839077155', '!1m16!1m12!1m3!1d29203.826346848637!2d90.37583692161202!3d23.801585363722065!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssuper+shop!5e0!3m2!1sen!2sbd!4v1510839299879', 1),
(2, 2, 'Apartment for Sale in Acme Casa Hermanas, Extended Pallabi', 0, 1150, 5, 3, 3, 5, 'yes', 'yes', 'no', 'yes', 'yes', 'd2.jpg', '!1m18!1m12!1m3!1d3649.8368486108984!2d90.35951131472919!3d23.824399984552944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ5JzI3LjgiTiA5MMKwMjEnNDIuMSJF!5e0!3m2!1sen!2sbd!4v1510982680526', '!1m14!1m12!1m3!1d108130.41418110872!2d90.30921675442512!3d23.832679338747802!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1510982941770', '!1m12!1m8!1m3!1d3649.870803723785!2d90.35905!3d23.8231928!3m2!1i1024!2i768!4f13.1!2m1!1sschool+and+college!5e0!3m2!1sen!2sbd!4v1510984043899', '!1m16!1m12!1m3!1d14599.873482772724!2d90.35939309718444!3d23.819723742918764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssuper+shop!5e0!3m2!1sen!2sbd!4v1510984423993', 1),
(3, 3, 'Apartment for Sale in Navana Probani Ridgedale (Block-1)', 0, 2246, 6, 3, 3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', 'd3.jpg', '!1m18!1m12!1m3!1d3650.0059099193504!2d90.3720057147291!3d23.81838888455594!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ5JzA2LjIiTiA5MMKwMjInMjcuMSJF!5e0!3m2!1sen!2sbd!4v1510985487142', '!1m16!1m12!1m3!1d29202.005542822644!2d90.36484428099614!3d23.809682913498175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1shospitals!5e0!3m2!1sen!2sbd!4v1510985425045', '!1m16!1m12!1m3!1d29200.047505200622!2d90.35668476559084!3d23.818387880672248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sschool+%26+colleges!5e0!3m2!1sen!2sbd!4v1510985538010', '!1m18!1m12!1m3!1d14600.0224206154!2d90.36544522623511!3d23.81839972200432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c12cc59b80bb%3A0x5a2b18163148107c!2zMjPCsDQ5JzA2LjIiTiA5MMKwMjInMjcuMSJF!5e0!3m2!1sen!2sbd!4v1511242261301', 1),
(4, 4, 'Apartment for Sale in IMAGINE EASTWOOD', 18500000, 2360, 6, 3, 4, 4, 'yes', 'yes', 'no', 'yes', 'yes', 'd4.jpg', '!1m18!1m12!1m3!1d3650.134945019258!2d90.42551131472896!3d23.8137999845582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ4JzQ5LjciTiA5MMKwMjUnMzkuNyJF!5e0!3m2!1sen!2sbd!4v1511242025434', '!1m18!1m12!1m3!1d14600.539809600748!2d90.41894522623356!3d23.813799722044987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64b98fe8201%3A0x86058b6e37eff9ed!2zMjPCsDQ4JzQ5LjciTiA5MMKwMjUnMzkuNyJF!5e0!3m2!1sen!2sbd!4v1511242071567', '!1m18!1m12!1m3!1d29201.11588857375!2d90.41036150244162!3d23.813638475069826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64b98fe8201%3A0x86058b6e37eff9ed!2zMjPCsDQ4JzQ5LjciTiA5MMKwMjUnMzkuNyJF!5e0!3m2!1sen!2sbd!4v1511242127921', '!1m18!1m12!1m3!1d43342.288625213296!2d90.39320609609705!3d23.78596782399727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64b98fe8201%3A0x86058b6e37eff9ed!2zMjPCsDQ4JzQ5LjciTiA5MMKwMjUnMzkuNyJF!5e0!3m2!1sen!2sbd!4v1511242193248', 1),
(5, 5, 'Apartment for Sale in Bijoy Rakeen City', 0, 1872, 5, 3, 3, 15, 'yes', 'yes', 'yes', 'yes', 'yes', 'd5.jpg', '', '', '', '', 1),
(6, 6, 'Apartment for Sale in Uttara', 0, 1900, 5, 3, 3, 2, 'yes', 'yes', 'yes', 'yes', 'yes', 'd6.jpg', '!1m18!1m12!1m3!1d3648.7502720790835!2d90.39961131473007!3d23.862999984534106!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDUxJzQ2LjgiTiA5MMKwMjQnMDYuNSJF!5e0!3m2!1sen!2sbd!4v1511241407770', '!1m18!1m12!1m3!1d14595.001117943819!2d90.39304522625022!3d23.862999721610002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c4266d049387%3A0x76f4a81e6383c60a!2zMjPCsDUxJzQ2LjgiTiA5MMKwMjQnMDYuNSJF!5e0!3m2!1sen!2sbd!4v1511241460280', '!1m18!1m12!1m3!1d22379.11467150865!2d90.38943400572447!3d23.86193061323032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c4266d049387%3A0x76f4a81e6383c60a!2zMjPCsDUxJzQ2LjgiTiA5MMKwMjQnMDYuNSJF!5e0!3m2!1sen!2sbd!4v1511241520258', '!1m18!1m12!1m3!1d29190.243436735364!2d90.38943399989128!3d23.8619294496687!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c4266d049387%3A0x76f4a81e6383c60a!2zMjPCsDUxJzQ2LjgiTiA5MMKwMjQnMDYuNSJF!5e0!3m2!1sen!2sbd!4v1511241586426', 1),
(7, 7, 'Apartment for Sale in Landmark JKH Complex', 0, 3844, 6, 4, 4, 1, 'yes', 'yes', 'yes', 'yes', 'yes', 'building.jpg', '!1m18!1m12!1m3!1d3650.621192511825!2d90.41061131472864!3d23.796499984566736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ3JzQ3LjQiTiA5MMKwMjQnNDYuMSJF!5e0!3m2!1sen!2sbd!4v1511241052171', '!1m18!1m12!1m3!1d14602.484799534543!2d90.40404522622772!3d23.796499722198327!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7a7894b9187%3A0xb144bd9a0dd8fc20!2zMjPCsDQ3JzQ3LjQiTiA5MMKwMjQnNDYuMSJF!5e0!3m2!1sen!2sbd!4v1511241113249', '!1m18!1m12!1m3!1d85809.34704422926!2d90.35434799511188!3d23.791113937134302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7a7894b9187%3A0xb144bd9a0dd8fc20!2zMjPCsDQ3JzQ3LjQiTiA5MMKwMjQnNDYuMSJF!5e0!3m2!1sen!2sbd!4v1511241207222', '!1m18!1m12!1m3!1d71622.5149188482!2d90.39583125834638!3d23.8185687262337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7a7894b9187%3A0xb144bd9a0dd8fc20!2zMjPCsDQ3JzQ3LjQiTiA5MMKwMjQnNDYuMSJF!5e0!3m2!1sen!2sbd!4v1511241330353', 1),
(8, 8, 'Apartment for Sale in Bijoy Rakeen City', 0, 1553, 5, 3, 3, 15, 'yes', 'yes', 'yes', 'yes', 'no', 'blue.jpg', '!1m18!1m12!1m3!1d3650.3963795453005!2d90.37941131472881!3d23.80449998456279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ4JzE2LjIiTiA5MMKwMjInNTMuOCJF!5e0!3m2!1sen!2sbd!4v1511240750153', '!1m18!1m12!1m3!1d29203.171261984444!2d90.36409036557215!3d23.80449898112301!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7292ca839bb%3A0x92442b86d47885d7!2zMjPCsDQ4JzE2LjIiTiA5MMKwMjInNTMuOCJF!5e0!3m2!1sen!2sbd!4v1511240827346', '!1m18!1m12!1m3!1d12325.906931095244!2d90.37584652579308!3d23.803317011261424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7292ca839bb%3A0x92442b86d47885d7!2zMjPCsDQ4JzE2LjIiTiA5MMKwMjInNTMuOCJF!5e0!3m2!1sen!2sbd!4v1511240905623', '!1m18!1m12!1m3!1d14601.718549555968!2d90.37584652402435!3d23.80331665926647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7292ca839bb%3A0x92442b86d47885d7!2zMjPCsDQ4JzE2LjIiTiA5MMKwMjInNTMuOCJF!5e0!3m2!1sen!2sbd!4v1511240960559', 1),
(9, 9, 'apartment for sale in Sufia South Point', 9462000, 1450, 5, 3, 4, 6, 'yes', 'yes', 'no', 'no', 'yes', 'bed.jpg', '!1m18!1m12!1m3!1d3649.9156031996977!2d90.42891131458177!3d23.821599984554318!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ5JzE3LjgiTiA5MMKwMjUnNTIuMCJF!5e0!3m2!1sen!2sbd!4v1511239971203', '!1m14!1m12!1m3!1d18608.220802740158!2d90.41610296656111!3d23.82113703862338!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1511240061410', '!1m18!1m12!1m3!1d14599.661193585089!2d90.42235632386054!3d23.821610821975707!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64803795937%3A0xf6bc2341aa1d6fae!2zMjPCsDQ5JzE3LjgiTiA5MMKwMjUnNTIuMCJF!5e0!3m2!1sen!2sbd!4v1511240302286', '!1m18!1m12!1m3!1d58398.64666496932!2d90.39609144272681!3d23.821606620591762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64803795937%3A0xf6bc2341aa1d6fae!2zMjPCsDQ5JzE3LjgiTiA5MMKwMjUnNTIuMCJF!5e0!3m2!1sen!2sbd!4v1511240476380', 1),
(10, 10, 'Apartment for sale in Priyoprangon Bashantika', 0, 1436, 5, 3, 3, 3, 'yes', 'yes', 'yes', 'yes', 'yes', 'sd.jpg', '!1m18!1m12!1m3!1d3651.7301632145595!2d90.46211131458037!3d23.756999984586198!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ1JzI1LjIiTiA5MMKwMjcnNTEuNSJF!5e0!3m2!1sen!2sbd!4v1511239667559', '!1m14!1m12!1m3!1d77988.72779242907!2d90.40198262746574!3d23.76178867857542!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1511239778784', '!1m16!1m12!1m3!1d116851.07861475134!2d90.40198254901878!3d23.76177461391217!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sschools+%26+colleges!5e0!3m2!1sen!2sbd!4v1511239838958', '!1m16!1m12!1m3!1d116851.1069650418!2d90.40198237290139!3d23.761743038970312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1ssuper+shop!5e0!3m2!1sen!2sbd!4v1511239895022', 1),
(11, 11, 'Apartment for Sale in Navana Probani Ridgedale (Block-5)', 0, 1411, 5, 3, 3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', 'din.jpg', '!1m18!1m12!1m3!1d3650.005597770499!2d90.37201131458166!3d23.8183999845559!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDQ5JzA2LjIiTiA5MMKwMjInMjcuMSJF!5e0!3m2!1sen!2sbd!4v1511238895301', '!1m18!1m12!1m3!1d14600.022420615427!2d90.36544522385941!3d23.81839972200408!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8496a10983c08898!2sNavana+Real+State!5e0!3m2!1sen!2sbd!4v1511238984153', '!1m18!1m12!1m3!1d29200.045008010246!2d90.35669035613813!3d23.818398980670715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c12cc59b80bb%3A0x5a2b18163148107c!2zMjPCsDQ5JzA2LjIiTiA5MMKwMjInMjcuMSJF!5e0!3m2!1sen!2sbd!4v1511239496838', '!1m18!1m12!1m3!1d29200.045452753435!2d90.35669034513697!3d23.818397003789862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c12cc59b80bb%3A0x5a2b18163148107c!2zMjPCsDQ5JzA2LjIiTiA5MMKwMjInMjcuMSJF!5e0!3m2!1sen!2sbd!4v1511239566488', 1),
(12, 12, '1675 sq-ft.(Type-B), 3 Bedroom Apartment for Sale in Civil Rahmat', 0, 1675, 5, 3, 3, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(13, 13, '1625 sq-ft., 3 Bedroom Apartment for sale in ABC Neel Kunja', 0, 1272, 5, 3, 3, 2, 'no', 'no', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(14, 14, '1128 sq-ft. (Type-D), 3 Bedroom Apartment for Sale in PPL HOSNE ARA VILLA', 0, 1128, 5, 3, 3, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(15, 15, '', 0, 1381, 6, 4, 3, 2, 'yes', 'yes', 'no ', 'yes', 'yes', '', '', '', '', '', 1),
(16, 16, '1500 sq-ft. (Type-E), 3 Bedroom Apartment for Sale in Joynal Tower', 0, 1500, 5, 3, 3, 8, 'yes', 'no', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(17, 17, '1640 sq-ft. (Type-C), 3 Bedroom Apartment for Sale in Toma Sunflower', 0, 1640, 6, 3, 3, 3, 'yes', 'yes', 'no', 'yes', 'yes', '', '', '', '', '', 1),
(18, 18, '1811 sq-ft. 3 bedroom full furnished Apartment', 18200000, 1811, 5, 3, 4, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(19, 19, '\r\n2090 sq-ft. 3 Bedroom Ready Apartment for Sale', 25080000, 2090, 5, 3, 3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', '+', '', '', '', '', 1),
(20, 20, '\r\n1054 sq-ft. (Type-A), 3 Bedroom Apartment for Sale in MAK Bhaban', 0, 1054, 5, 3, 3, 7, 'yes', 'yes', 'no', 'yes', 'yes', '', '', '', '', '', 1),
(21, 21, '\r\n2644 sq-ft. 4 Bedroom Luxurious Apartment for Sale ', 32000000, 2644, 7, 4, 4, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(22, 22, '2400 sq-ft. 3 bedroom Apartment for sale in Bay\'s Brookwood', 43200000, 2400, 5, 3, 3, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(23, 23, '1100 sq-ft. (Type-A), 3 Bedroom Apartment for Sale in Model Dewan Monjil', 0, 110, 5, 3, 3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(24, 24, '\r\n2400 sq-ft. 3 Bedroom Luxury Apartment for Sale', 50000000, 2400, 5, 3, 3, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '', '', '', '', '', 1),
(25, 25, '\r\n1945 sq-ft. 3 Bedroom Apartment for sale', 45000000, 1945, 4, 3, 3, 4, 'yes', 'yes', 'no', 'yes', 'yes', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_verified` varchar(10) NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `is_verified`) VALUES
(2, 'uchchash', '123456789', 'shrabon56@gmail.com', 'Yes'),
(3, 'shemonti', '121234345656', 'shemonti@gmail.com', 'Yes'),
(4, 'samiran', '12345678', 'samiran@icddrb.org', 'Yes'),
(5, 'antor', '12123456', 'antor@yahoo.com', 'Yes'),
(6, 'amin', '11111111', 'dfklgjfskl@dsjfkljdhasklj.com', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE `user_preferences` (
  `id` int(100) NOT NULL,
  `user_id` int(100) NOT NULL,
  `hospitals` double NOT NULL,
  `environmental_surroundings` double NOT NULL,
  `roads` double NOT NULL,
  `electricity` double NOT NULL,
  `gas` double NOT NULL,
  `departmental_store` double NOT NULL,
  `school_colleges` double NOT NULL,
  `super_market` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_preferences`
--

INSERT INTO `user_preferences` (`id`, `user_id`, `hospitals`, `environmental_surroundings`, `roads`, `electricity`, `gas`, `departmental_store`, `school_colleges`, `super_market`) VALUES
(1, 2, 3.4, 3.6, 2.2, 2.8, 4.4, 3.2, 2.4, 1.8),
(2, 5, 3, 3, 4.5, 2, 3, 4, 4, 3),
(3, 3, 3.67, 4.67, 4, 3.67, 3.67, 2.67, 3, 4),
(4, 4, 3, 3, 2.5, 3, 3, 4.5, 4, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `historical data`
--
ALTER TABLE `historical data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_id` (`p_id`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `INDEX` (`d_id`);

--
-- Indexes for table `poi`
--
ALTER TABLE `poi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real_estate`
--
ALTER TABLE `real_estate`
  ADD PRIMARY KEY (`rs_id`),
  ADD KEY `Index` (`p_id`),
  ADD KEY `d_id` (`d_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `rs_id` (`rs_id`);

--
-- Indexes for table `rs_details`
--
ALTER TABLE `rs_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rs_id` (`rs_id`),
  ADD KEY `d_id` (`d_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `d_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `historical data`
--
ALTER TABLE `historical data`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
  MODIFY `p_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `poi`
--
ALTER TABLE `poi`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=786;

--
-- AUTO_INCREMENT for table `real_estate`
--
ALTER TABLE `real_estate`
  MODIFY `rs_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `rs_details`
--
ALTER TABLE `rs_details`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_preferences`
--
ALTER TABLE `user_preferences`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `historical data`
--
ALTER TABLE `historical data`
  ADD CONSTRAINT `historical data_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `place` (`p_id`);

--
-- Constraints for table `real_estate`
--
ALTER TABLE `real_estate`
  ADD CONSTRAINT `real_estate_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `place` (`p_id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`rs_id`) REFERENCES `real_estate` (`rs_id`);

--
-- Constraints for table `rs_details`
--
ALTER TABLE `rs_details`
  ADD CONSTRAINT `rs_details_ibfk_1` FOREIGN KEY (`rs_id`) REFERENCES `real_estate` (`rs_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
