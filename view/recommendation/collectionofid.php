<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\recommendation\recommendation;


$auth= new recommendation();

$status= $auth->setData($_POST)->collectid();
$_SESSION['name']=$_POST['Name'];
//var_dump($status);
foreach($status as $all) {
    $value = $all->p_id;
}
//var_dump($value);
$list=$auth->lists($value);
//var_dump($list);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Template</title>
    <link rel="stylesheet" href="../../Resources/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="style.css"> -->

    <script src="../../Resources/bootstrap/js/jquery-3.2.0.min.js"></script>
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">

    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            width: 70%;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        .container1 {

            padding: 2px 16px;
        }
    </style>


</head>
<body>
<div>
    <div class="header">
        <div class="container">
            <b>
                R E A L - E S T A T O R
            </b>

            <div class=navbar-right>
                <a href="login.php" class="btn btn-outline-info my-2 my-sm-0">Login</a>
                <a href="signup.php" class="btn btn-outline-info my-2 my-sm-0">Sign Up</a>
            </div>
        </div>

    </div>

    <div class="middle" style="color: #2aabd2">

        <nav class="navbar navbar-inverse">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <form class="navbar-form navbar-right" action="collectionofid.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="Name">
                        </div>
                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-left">
                        <li ><a href="#" class="hi">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Buy</a></li>
                        <li><a href="#">Rent</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Area <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Dhaka</a></li>
                                <li><a href="#">Chittagong</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


        <!--        --><?php
        //
        //        function distance() {
        //
        //            $pi80 = M_PI / 180;
        //            $lat1=23.765765;
        //            $lat2=23.751341;
        //            $lon1=90.76590 ;
        //            $lon2=90.368849 ;
        //            $lat1 *= $pi80;
        //            $lon1 *= $pi80;
        //            $lat2 *= $pi80;
        //            $lon2 *= $pi80;
        //
        //            $r = 6372.797; // mean radius of Earth in km
        //            $dlat = $lat2 - $lat1;
        //            $dlon = $lon2 - $lon1;
        //            $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        //            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        //            $km = $r * $c;
        //
        //            //echo '<br/>'.$km;
        //            return $km;
        //        }
        //        ?>
        <!--        --><?php
        //        echo distance();
        //        ?>
        <?php
        echo "<h2 align='center' style='color: #5bc0de'>Recommendations for ".$_SESSION['name']."<br></h2>"
        ?>


        <?php
        $array_save=array();
        $array_img=array();
        $array_id=array();
        $poi=array();
        $hi=array();
        $u=array();
        $s=array();
        $b=array();
        $r=array();

            foreach ($list as $deta)
            {
                $array_save[]=$deta->name;
                $array_img[]=$deta->image;
                $array_id[]=$deta->rs_id;

            }
            for($i=0;$i<sizeof($array_save);$i++)
            {
                $poi[]=$auth->pointofinterest($array_id[$i]);

            }

          for($j=0;$j<10;$j++)
          {
              $u[$j]=0;
              $s[$j]=0;
              $b[$j]=0;
              $r[$j]=0;

              $save = $poi[$j];
//              var_dump($save);
              foreach ($save as $item) {
                  $hi[] = $item->types;
              }
//              var_dump($hi);

              for ($i = 0; $i < sizeof($hi); $i++) {
                  if ($hi[$i] == "university")
                      $u[$j]=$u[$j]+1;
                  elseif ($hi[$i] == "school")
                      $s[$j]=$s[$j]+1;
                  elseif ($hi[$i] == "bank")
                      $b[$j]=$b[$j]+1;
                  elseif ($hi[$i] == "restaurant")
                      $r[$j]=$r[$j]+1;


              }
              unset($hi);

//
          }



//echo "$u[1]";


        //
        //$h=implode(', ', $array_id);
        //
        //$ha=explode(" ",$h);
        //var_dump($ha);

        for($d=0;$d<sizeof($array_save);$d++)
        {
            $e=$d+1;

            $f=$d+2;

            $g=$d+3;


            echo"

            <br>
            <div class='col-lg-12' style='color: #5bc0de;margin-bottom: 50px'>
            <div class='col-lg-3' >
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$array_img[$d]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $array_save[$d] <br>
            $u[$d] Universities and $s[$d] Schools<br>
            $b[$d] banks<br>
            $r[$d] restaurants
            </div>
            </div>
            </div>
            <div class='col-lg-3'>
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$array_img[$e]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $array_save[$e]<br>
            $u[$e] Universities and $s[$e] Schools<br>
            $b[$e] banks<br>
            $r[$e] restaurants
             
            </div>
            </div>
            </div>
            <div class='col-lg-3'>
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$array_img[$f]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $array_save[$f]<br>
            $u[$f] Universities and $s[$f] Schools<br>
            $b[$f] banks<br>
            $r[$f] restaurants
            </div>
            </div>
            </div>
            <div class='col-lg-3'>
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$array_img[$g]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $array_save[$g]
            
            <br>
            $u[$g] Universities and $s[$g] Schools
            <br>
            $b[$g] banks<br>
            $r[$g] restaurants
            </div>
            </div>
            </div>
            </div>
            
                      ";
            $d=$d+3;

        }


        ?>

    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


    <div class="footer">
        <div class="container">
            <div class="col-lg-4">
                <h3>About</h3>
                <hr>
                <br>
                <p>About us</p><br>
                <p>Term of trade</p><br>
                <p>Privacy policy</p><br>
                <p>Copyright</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Apartment for rent</h3>
                <hr>
                <br>
                <p>Dhaka</p><br>
                <p>Chittagong</p><br>
                <p>Gulshan</p><br>
                <p>Dhanmondi</p><br>
                <p>Banani</p><br>
                <p>Uttara</p><br>
                <p>Agrabad</p><br>
                <p>Nasirabad</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Contact with us</h3>
                <hr>
                <a href="www.facebook.com"><img src="../../Resources/images/facebook.png"></a>
                <a href="www.twitter.com"><img src="../../Resources/images/twitter.png"></a>
                <a href="www.google.com"><img src="../../Resources/images/google_plus.png"></a>
                <a href="www.linkedin.com"><img src="../../Resources/images/linkedin.png"></a>
            </div>
        </div>
    </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>


<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>

</body>
</html>
