<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\recommendation\recommendation;
$auth= new recommendation();

$all=$auth->gulshan();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Template</title>
    <link rel="stylesheet" href="../../Resources/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="style.css"> -->

    <script src="../../Resources/bootstrap/js/jquery-3.2.0.min.js"></script>
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">


</head>
<body>
<div>
    <div class="header">
        <div class="container">
            <b>
                R E A L - E S T A T O R
            </b>


        </div>

    </div>

    <div class="middle">

        <nav class="navbar navbar-inverse">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <form class="navbar-form navbar-right" action="#" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="Name">
                        </div>
                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-left">
                        <li ><a href="#" class="hi">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Buy</a></li>
                        <li><a href="#">Rent</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Area <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Dhaka</a></li>
                                <li><a href="#">Chittagong</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


        <h1 style="color: #5bc0de;text-align: center">Apartments in dhaka</h1>
        <div class="navbar container">
            <a href='mirpur.php' class='btn btn-lg bg-success'>Mirpur</a>
            <a href='#' class='btn btn-lg bg-danger'>Dhanmondi</a>
            <a href='Uttara.php' class='btn btn-lg bg-danger'>Uttara</a>
            <button id="DeleteSelected" class='btn btn-lg bg-danger'>Bashundhara</button>
            <a href='gulshan.php' class='btn btn-lg bg-primary'>Gulshan</a>
            <a href='#' class='btn btn-lg bg-danger'>Malibag</a>

        </div>
        <?php

        $dhakanames=array();
        $dhakaimages=array();

        foreach($all as $dhaka)
        {
            $dhakanames[]=$dhaka->name;
            $dhakaimages[]=$dhaka->image;
        }
        for($d=0;$d<sizeof($dhakanames);$d++)
        {

            $e=$d+1;

            $f=$d+2;

            $g=$d+3;
            echo"

            <br>
            <div class='col-lg-12' style='color: #5bc0de;margin-bottom: 50px'>
            <div class='col-lg-3' >
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$dhakaimages[$d]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $dhakanames[$d] <br>
            
            </div>
            </div>
            </div>
            <div class='col-lg-3'>
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$dhakaimages[$e]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $dhakanames[$e]<br> 
            </div>
            </div>
            </div>
            <div class='col-lg-3'>
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$dhakaimages[$f]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $dhakanames[$f]<br>
            </div>
            </div>
            </div>
            <div class='col-lg-3'>
            <div class='card' style='margin-left: 70px;'>
            
            <img src='../../Resources/images/$dhakaimages[$g]' style='width: 210px;height: 200px;'><br>
            <div class='container1'>
            $dhakanames[$g]
            
            <br>
            </div>
            </div>
            </div>
            </div>
            
                      ";
            $d=$d+3;

        }


        ?>







        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


        <div class="footer">
            <div class="container">
                <div class="col-lg-4">
                    <h3>About</h3>
                    <hr>
                    <br>
                    <p>About us</p><br>
                    <p>Term of trade</p><br>
                    <p>Privacy policy</p><br>
                    <p>Copyright</p><br>
                </div>
                <div class="col-lg-4">
                    <h3>Apartment for rent</h3>
                    <hr>
                    <br>
                    <p>Dhaka</p><br>
                    <p>Chittagong</p><br>
                    <p>Gulshan</p><br>
                    <p>Dhanmondi</p><br>
                    <p>Banani</p><br>
                    <p>Uttara</p><br>
                    <p>Agrabad</p><br>
                    <p>Nasirabad</p><br>
                </div>
                <div class="col-lg-4">
                    <h3>Contact with us</h3>
                    <hr>
                    <a href="www.facebook.com"><img src="../../Resources/images/facebook.png"></a>
                    <a href="www.twitter.com"><img src="../../Resources/images/twitter.png"></a>
                    <a href="www.google.com"><img src="../../Resources/images/google_plus.png"></a>
                    <a href="www.linkedin.com"><img src="../../Resources/images/linkedin.png"></a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>


    <script>
        $(document).ready(function(){
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $('#back-to-top').tooltip('show');

        });
    </script>

</body>
</html>