<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Template</title>
    <link rel="stylesheet" href="../../Resources/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="style.css"> -->

    <script src="../../Resources/bootstrap/js/jquery-3.2.0.min.js"></script>
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css">


</head>
<body>
<div>
    <div class="header">
        <div class="container">
            <b>
                R E A L - E S T A T O R
            </b>

            <div class=navbar-right>
                <a href="login.php" class="btn btn-outline-info my-2 my-sm-0">Login</a>
                <a href="signup.php" class="btn btn-outline-info my-2 my-sm-0">Sign Up</a>
            </div>
        </div>

    </div>

    <div class="middle">

        <nav class="navbar navbar-inverse">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <form class="navbar-form navbar-right" action="collectionofid.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search" name="Name">
                        </div>
                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-left">
                        <li ><a href="#" class="hi">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Buy</a></li>
                        <li><a href="#">Rent</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Area <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="dhaka.php">Dhaka</a></li>
                                <li><a href="#">Chittagong</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


<!--        --><?php
//
//        function distance() {
//
//            $pi80 = M_PI / 180;
//            $lat1=23.765765;
//            $lat2=23.751341;
//            $lon1=90.76590 ;
//            $lon2=90.368849 ;
//            $lat1 *= $pi80;
//            $lon1 *= $pi80;
//            $lat2 *= $pi80;
//            $lon2 *= $pi80;
//
//            $r = 6372.797; // mean radius of Earth in km
//            $dlat = $lat2 - $lat1;
//            $dlon = $lon2 - $lon1;
//            $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
//            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
//            $km = $r * $c;
//
//            //echo '<br/>'.$km;
//            return $km;
//        }
//        ?>
<!--        --><?php
//        echo distance();
//        ?>


        <div class="slider">
            <div class="container">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="../../Resources/images/dhaka.jpg" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="../../Resources/images/banglow.jpg" alt="Second slide">
                        </div>
                        <div class="item">
                            <img src="../../Resources/images/moru.jpg" alt="Third slide">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="diffitem" >
            <div class="container">
                <div class="col-md-4" style="text-align: center">
                    <img src="../../Resources/images/28.jpg" style="border-radius: 100%">
                    <h3>Latest</h3>
                    <p style="text-align: justify">Looking for your dream home? Well, look no further, because Lamudi has what you are looking for! With our busy lives, it is key to have a reliable source when looking to invest in property, which is where our platform comes into play. Lamudi is the country's best online property finder, providing customers with a wide selection of apartments, houses, plots of land and commercial properties for sale and for rent. Individual sellers can also sell their properties in a secure way. Lamudi allows you to choose the desired accommodation and commercial property via trusted real estate agents on a guaranteed secure platform. Go ahead and try it now!</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="../../Resources/images/bed.jpg" style="border-radius: 100%">
                    <h3>High quality Properties</h3>
                    <p style="text-align: justify">Looking for your dream home? Well, look no further, because Lamudi has what you are looking for! With our busy lives, it is key to have a reliable source when looking to invest in property, which is where our platform comes into play. Lamudi is the country's best online property finder, providing customers with a wide selection of apartments, houses, plots of land and commercial properties for sale and for rent. Individual sellers can also sell their properties in a secure way. Lamudi allows you to choose the desired accommodation and commercial property via trusted real estate agents on a guaranteed secure platform. Go ahead and try it now!</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="../../Resources/images/building.jpg" style="border-radius: 100%">
                    <h3>Recommendation</h3>
                    <p style="text-align: justify">Looking for your dream home? Well, look no further, because Lamudi has what you are looking for! With our busy lives, it is key to have a reliable source when looking to invest in property, which is where our platform comes into play. Lamudi is the country's best online property finder, providing customers with a wide selection of apartments, houses, plots of land and commercial properties for sale and for rent. Individual sellers can also sell their properties in a secure way. Lamudi allows you to choose the desired accommodation and commercial property via trusted real estate agents on a guaranteed secure platform. Go ahead and try it now!</p>
                </div>
            </div>
        </div>


        <div class="formforus">
            <div class="container">
                <div class="form-group">
                    <form class="form-horizontal well ">
                        <fieldset>
                            <legend class="text-center">Help us to recommend you</legend>
                            <div class="form-group row">
                                <label for="inputfstnm" class="col-sm-2 col-form-label">Firstname</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputfstnm" placeholder="Firstname">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputlasname" class="col-sm-2 col-form-label">Lastname</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputlasname" placeholder="Lastname">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputaddress" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputaddress" placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputcity" class="col-sm-2 col-form-label">city</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputcity" placeholder=city>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputstate" class="col-sm-2 col-form-label">state</label>
                                <div class="col-sm-10">
                                    <select name="state" class="form-control selectpicker" id="inputstate">
                                        <option value=" " >Please select your state</option>
                                        <option>Dhaka</option>
                                        <option>Chittagong</option>
                                        <option >Sylhet</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputzip" class="col-sm-2 col-form-label">Zip</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputzip" placeholder="Zip">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 "></label>

                                <div class="col-lg-4 position">
                                    <button type="submit" class="btn-default" value="Submit" style="height: 30px;width: 100px;text-align: center">
                                        Submit
                                    </button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


    <div class="footer">
        <div class="container">
            <div class="col-lg-4">
                <h3>About</h3>
                <hr>
                <br>
                <p>About us</p><br>
                <p>Term of trade</p><br>
                <p>Privacy policy</p><br>
                <p>Copyright</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Apartment for rent</h3>
                <hr>
                <br>
                <p>Dhaka</p><br>
                <p>Chittagong</p><br>
                <p>Gulshan</p><br>
                <p>Dhanmondi</p><br>
                <p>Banani</p><br>
                <p>Uttara</p><br>
                <p>Agrabad</p><br>
                <p>Nasirabad</p><br>
            </div>
            <div class="col-lg-4">
                <h3>Contact with us</h3>
                <hr>
                <a href="www.facebook.com"><img src="../../Resources/images/facebook.png"></a>
                <a href="www.twitter.com"><img src="../../Resources/images/twitter.png"></a>
                <a href="www.google.com"><img src="../../Resources/images/google_plus.png"></a>
                <a href="www.linkedin.com"><img src="../../Resources/images/linkedin.png"></a>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>


<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>

</body>
</html>